<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!--
 // WEBSITE: https://themefisher.com
 // TWITTER: https://twitter.com/themefisher
 // FACEBOOK: https://www.facebook.com/themefisher
 // GITHUB: https://github.com/themefisher/
-->
<html lang="en-us">

<head>
   <meta charset="utf-8">
   <title>Consola - Lemuria</title>

   <!-- mobile responsive meta -->
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
   <meta name="description" content="This is meta description">
   <meta name="author" content="Themefisher">
 
   <!-- theme meta -->
   <meta name="theme-name" content="logbook" />

   <!-- plugins -->
   <link rel="preload" href="https://fonts.gstatic.com/s/opensans/v18/mem8YaGs126MiZpBA-UFWJ0bbck.woff2" style="font-display: optional;">
   <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:600%7cOpen&#43;Sans&amp;display=swap" media="screen">

   <link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
   <link rel="stylesheet" href="plugins/slick/slick.css">

   <!-- Main Stylesheet -->
   <link rel="stylesheet" href="css/style.css">

   <!--Favicon-->
   <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
   <link rel="icon" href="images/favicon.png" type="image/x-icon">
</head>

<body>
<!-- navigation -->
<header class="sticky-top bg-white border-bottom border-default">
   <div class="container">
	<%@include file="menu.html"%>      
   </div>
</header>
<!-- /navigation -->

<section class="section">
	<div class="container">
		<div class="row">
			<div class="col-lg-8  mb-5 mb-lg-0">
				<article class="row mb-5">
					<div class="col-12">
						<div class="post-slider">
							<img loading="lazy" src="images/post/post-6.jpg" class="img-fluid" alt="post-thumb">
							<img loading="lazy" src="images/post/post-1.jpg" class="img-fluid" alt="post-thumb">
							<img loading="lazy" src="images/post/post-3.jpg" class="img-fluid" alt="post-thumb">
							<img loading="lazy" src="images/post/post-8.jpg" class="img-fluid" alt="post-thumb">
						</div>
					</div>
					<div class="col-12 mx-auto">
						<h3><a class="post-title" href="post-elements.html">Elementos del juego que podemos administrar</a></h3>
						<ul class="list-inline post-meta mb-4">
							<li class="list-inline-item"><i class="ti-user mr-2"></i>
								<a href="author.html">fchavez19</a>
							</li>
							<li class="list-inline-item">Fecha : Abril 26, 2023</li>
							<li class="list-inline-item">Categorias : <a href="#!" class="ml-1">Secciones </a>
							</li>
							<li class="list-inline-item">Etqs : <a href="#!" class="ml-1">Seccion </a> ,<a href="#!" class="ml-1">Encabezado </a>
							</li>
						</ul>
						<p>
                                                  Estos son elementos que pueden añadirse de forma dinámica al juego de forma que tenga variabilidad y sea menos monotono.
                                                </p> <!--<a href="#" class="btn btn-outline-primary">Consultar</a>-->
					</div>
				</article>
				<article class="row mb-5">
					<div class="col-12">
						<div class="post-slider">
							<img loading="lazy" src="images/post/post-1.jpg" class="img-fluid" alt="post-thumb">
						</div>
					</div>
					<div class="col-12 mx-auto">
						<h3><a class="post-title" href="salas.html">El mapa está compuesto por un conjunto de salas</a></h3>
						<ul class="list-inline post-meta mb-4">
							<li class="list-inline-item"><i class="ti-user mr-2"></i><a href="author.html">fchavez19</a>
							</li>
							<li class="list-inline-item">Fecha : Abril 26, 2023</li>
							<li class="list-inline-item">Categorias : <a href="#!" class="ml-1">Salas </a>
							</li>
							<li class="list-inline-item">Etqs : <a href="#!" class="ml-1">Sala </a> ,<a href="#!" class="ml-1">Mapa </a>
							</li>
						</ul>
						<p>
                                                  Las salas son los lugares en donde están los items, desafortunadamente para el heroe, los items están protegidos 
                                                  por alimañas y enemigos de diversa indole y poder.
                                                </p> <a href="salas.html" class="btn btn-outline-primary">Consultar</a>
					</div>
				</article>
				<article class="row mb-5">
					<div class="col-12">
						<div class="post-slider">
							<img loading="lazy" src="images/post/post-2.jpg" class="img-fluid" alt="post-thumb">
							<img loading="lazy" src="images/post/post-4.jpg" class="img-fluid" alt="post-thumb">
						</div>
					</div>
					<div class="col-12 mx-auto">
						<h3>El cambio de estos elementos puede lograr que el juego tenga un nuevo tema</h3>
						<ul class="list-inline post-meta mb-4">
							<li class="list-inline-item"><i class="ti-user mr-2"></i><a href="author.html">fchavez19</a>
							</li>
							<li class="list-inline-item">Fecha : Abril 26, 2023</li>
							<li class="list-inline-item">Categorias : <a href="#!" class="ml-1">Pociones </a>
							</li>
							<li class="list-inline-item">Etqs : <a href="#!" class="ml-1">Poción </a> ,<a href="#!" class="ml-1">Cambio </a>
							</li>
						</ul>
						<p>
                                                 Un conjunto de escenarios, enemigos y armas diferentes puede darle al juego un tema nuevo. Por ejemplo, podriamos llenar 
                                                 con escenarios acuaticos, animales acuaticos como enemigos y nuevas armas para que el mapa sugiera que todo pasa dentro
                                                 del agua. 
                                                </p> <!--<a href="post-details-2.html" class="btn btn-outline-primary">Consultar</a>-->
					</div>
				</article>
				<article class="row mb-5">
					<div class="col-12">
						<div class="post-slider">
							<img loading="lazy" src="images/post/post-3.jpg" class="img-fluid" alt="post-thumb">
						</div>
					</div>
					<div class="col-12 mx-auto">
						<h3><a class="post-title" href="pociones.html">Las pociones sirven para variar la posibilidad de triunfo en las batallas</a></h3>
						<ul class="list-inline post-meta mb-4">
							<li class="list-inline-item"><i class="ti-user mr-2"></i><a href="author.html">John Doe</a>
							</li>
							<li class="list-inline-item">Date : March 14, 2020</li>
							<li class="list-inline-item">Categories : <a href="#!" class="ml-1">Videography </a>
							</li>
							<li class="list-inline-item">Tags : <a href="#!" class="ml-1">Video </a> ,<a href="#!" class="ml-1">Image </a>
							</li>
						</ul>
						<p>
                                                  Los combates pueden debilitarte asi que las pociones te brindan la oportunidad de recuperarte para las siguentes batallas. Existen tres tipos
                                                 de pociones: Vida, Defensa y Ataque.
                                                </p> <a href="pociones.html" class="btn btn-outline-primary">Consultar</a>
					</div>
				</article>
				<article class="row mb-5">
					<div class="col-12">
						<div class="post-slider">
							<img loading="lazy" src="images/post/post-5.jpg" class="img-fluid" alt="post-thumb">
							<img loading="lazy" src="images/post/post-7.jpg" class="img-fluid" alt="post-thumb">
						</div>
					</div>
					<div class="col-12 mx-auto">
						<h3>Proximos pasos en el juego</h3>
						<ul class="list-inline post-meta mb-4">
							<li class="list-inline-item"><i class="ti-user mr-2"></i><a href="author.html">fchavez19</a>
							</li>
							<li class="list-inline-item">Fecha : Abril 26, 2023</li>
							<li class="list-inline-item">Categorias : <a href="#!" class="ml-1">Futuro </a>
							</li>
							<li class="list-inline-item">Etqs : <a href="#!" class="ml-1">Evolución </a>
								,<a href="#!" class="ml-1">Nuevas características </a>
							</li>
						</ul>
						<p>
                                                 El juego debe evolucionar, los dos aspectos en los que esperamos trabajar pronto son: la animación de las batllas y el poder organizar
                                                 torneos. 
                                                </p> <!--<a href="post-details-1.html" class="btn btn-outline-primary">Consultar</a>-->
					</div>
				</article>
				<article class="row mb-5">
					<div class="col-12">
						<div class="post-slider">
							<img loading="lazy" src="images/post/post-6.jpg" class="img-fluid" alt="post-thumb">
						</div>
					</div>
					<div class="col-12 mx-auto">
						<h3><a class="post-title" href="cargaArmas">Las armas son necesarias para pelear contra ciertos enemigos</a></h3>
						<ul class="list-inline post-meta mb-4">
							<li class="list-inline-item"><i class="ti-user mr-2"></i><a href="author.html">fchavez19</a>
							</li>
							<li class="list-inline-item">Fecha : Abril 26, 2023</li>
							<li class="list-inline-item">Categorias : <a href="#!" class="ml-1">Armas </a>
							</li>
							<li class="list-inline-item">Etqs : <a href="#!" class="ml-1">Arma </a> ,<a href="#!" class="ml-1">Potencia </a>
							</li>
						</ul>
						<p>
                                                 Algunos de los enemigos que enfrentarás son demasiado poderosos como para enfrentarlos solo con tus manitas, asi
                                                 que el juego tiene armas que pueden potenciar tu nivel de ataque.
                                                </p> <a href="cargaArmas" class="btn btn-outline-primary">Consultar</a>
					</div>
				</article>
				<article class="row mb-5">
					<div class="col-12">
						<div class="post-slider">
							<img loading="lazy" src="images/post/post-8.jpg" class="img-fluid" alt="post-thumb">
						</div>
					</div>
					<div class="col-12 mx-auto">
						<h3><a class="post-title" href="enemigos.html">Los enemigos protegen los items dentro de la sala, sólo venciendolos puedes recoger el item</a></h3>
						<ul class="list-inline post-meta mb-4">
							<li class="list-inline-item"><i class="ti-user mr-2"></i><a href="author.html">fchavez19</a>
							</li>
							<li class="list-inline-item">Fecha : Abril 26, 2023</li>
							<li class="list-inline-item">Categorias : <a href="#!" class="ml-1">Armas </a>
							</li>
							<li class="list-inline-item">Etqs : <a href="#!" class="ml-1">Enemigo </a> ,<a href="#!" class="ml-1">Pelea </a>
							</li>
						</ul>
						<p>
                                                 Los enemigos tienen la misión de proteger los items dentro de la sala, de hecho mientras exista un enemigo dentro de la sala 
                                                 ni siquiera puedes ver el item dentro de la sala.
                                                </p> <a href="enemigos.html" class="btn btn-outline-primary">Consultar</a>
					</div>
				</article>
				<article class="row mb-5">
					<div class="col-12 mx-auto">
						<h3><a class="post-title" href="post-details-2.html">Lemuria (la tierra de los lemures)</a></h3>
						<ul class="list-inline post-meta mb-4">
							<li class="list-inline-item"><i class="ti-user mr-2"></i><a href="author.html">fchavez19</a>
							</li>
							<li class="list-inline-item">fecha : Abril 26, 2023</li>
							<li class="list-inline-item">Categorias : <a href="#!" class="ml-1">Introducción</a>
							</li>
							<li class="list-inline-item">Etqs : <a href="#!" class="ml-1">Lemur </a> ,<a href="#!" class="ml-1">Lemuria </a>
							</li>
						</ul>
						<p>
                                                  En la mitología romana los lemures eran espectros o espíritus de la muerte; eran la versión maligna de los lares. Los lemures 
                                                  vagaban por la noche, atormentaban y asustaban a los vivos
                                                </p> <!--<a href="post-details-2.html" class="btn btn-outline-primary">Consultar</a>-->
					</div>
				</article>
			</div>
			<aside class="col-lg-4">
				   <!-- Search -->
   <div class="widget">
      <h5 class="widget-title"><span>Buscar</span></h5>
      <form action="/logbook-hugo/search" class="widget-search">
         <input id="search-query" name="s" type="search" placeholder="Que deseas buscar?...">
         <button type="submit"><i class="ti-search"></i>
         </button>
      </form>
   </div>
   <!-- categories -->
   <div class="widget">
      <h5 class="widget-title"><span>Grupos</span></h5>
      <ul class="list-unstyled widget-list">
         <li><a href="para_I_22B.html" class="d-flex">Paradigmas I 
               <small class="ml-auto">(2022 B)</small></a>
         </li>
         <li><a href="para_I_23A.html" class="d-flex">Paradigmas I 
               <small class="ml-auto">(2023 A)</small></a>
         </li>
         <li><a href="software_II_23A.html" class="d-flex">Ing. de Software II 
               <small class="ml-auto">(2023 A)</small></a>
         </li>
         <li><a href="diseno_23A.html" class="d-flex">Diseño de Sistemas 
               <small class="ml-auto">(2023 A)</small></a>
         </li>
      </ul>
   </div>
   <!-- tags -->
   <div class="widget">
      <h5 class="widget-title"><span>Etiquetas</span></h5>
      <ul class="list-inline widget-list-inline">
         <li class="list-inline-item"><a href="#!">Juego</a>
         </li>
         <li class="list-inline-item"><a href="#!">Proyecto</a>
         </li>
         <li class="list-inline-item"><a href="#!">Lemuria</a>
         </li>
         <li class="list-inline-item"><a href="#!">Configuración</a>
         </li>
         <li class="list-inline-item"><a href="#!">Git</a>
         </li>
         <li class="list-inline-item"><a href="#!">Java</a>
         </li>
         <li class="list-inline-item"><a href="#!">Patrones</a>
         </li>
      </ul>
   </div>
   <!-- latest post -->
   <div class="widget">
      <h5 class="widget-title"><span>Secciones</span></h5>
      <!-- post-item -->
      <ul class="list-unstyled widget-list">
         <li class="media widget-post align-items-center">
            <a href="post-elements.html">
               <img loading="lazy" class="mr-3" src="images/post/post-6.jpg">
            </a>
            <div class="media-body">
               <h5 class="h6 mb-0"><a href="cargaArmas">
                 Potenciadores de ataque
               </a></h5>
               <small>Abril 26, 2023</small>
            </div>
         </li>
      </ul>
      <ul class="list-unstyled widget-list">
         <li class="media widget-post align-items-center">
            <a href="post-details-1.html">
               <img loading="lazy" class="mr-3" src="images/post/post-1.jpg">
            </a>
            <div class="media-body">
               <h5 class="h6 mb-0"><a href="salas.html">Localidades donde están los items</a>
               </h5>
               <small>Abril 26, 2023</small>
            </div>
         </li>
      </ul>
      <ul class="list-unstyled widget-list">
         <li class="media widget-post align-items-center">
            <a href="post-details-2.html">
               <img loading="lazy" class="mr-3" src="images/post/post-3.jpg">
            </a>
            <div class="media-body">
               <h5 class="h6 mb-0"><a href="pociones.html">Medios para restablecer niveles</a>
               </h5>
               <small>Abril 26, 2023</small>
            </div>
         </li>
      </ul>
      <ul class="list-unstyled widget-list">
         <li class="media widget-post align-items-center">
            <a href="post-details-2.html">
               <img loading="lazy" class="mr-3" src="images/post/post-8.jpg">
            </a>
            <div class="media-body">
               <h5 class="h6 mb-0"><a href="pociones.html">Medios para restablecer niveles</a>
               </h5>
               <small>Abril 26, 2023</small>
            </div>
         </li>
      </ul>
   </div>
			</aside>
		</div>
	</div>
</section>
<%@include file="pie.html"%>

   <!-- JS Plugins -->
   <script src="plugins/jQuery/jquery.min.js"></script>
   <script src="plugins/bootstrap/bootstrap.min.js" async></script>
   <script src="plugins/slick/slick.min.js"></script>

   <!-- Main Script -->
   <script src="js/script.js"></script>
</body>
</html>
