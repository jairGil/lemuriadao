package mx.uaemex.fi.ing_software_ii.lemuria.control;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class CargadorDeArmas
 */
public class CargadorDeArmas extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ArmasDAO dao;
	private DataSource ds;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		
		InitialContext cxt;
		
		try {
			cxt=new InitialContext();
			if(cxt!=null) {
				this.ds=(DataSource)cxt.lookup("java:/comp/env/jdbc/ds");
			}
			if(this.ds==null) {
				throw new ServletException("DataSource no econtrado");
			}
		}catch(NamingException e) {
			throw new ServletException("Sin contexto inicial");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArmasDAODerbyImp realDao;
		Connection con;
		List<Arma> res;
		HttpSession sesion;
		
		realDao = new ArmasDAODerbyImp();
		try {
			con = this.ds.getConnection();
			realDao.setConexion(con);
			this.dao = realDao;
			res = this.dao.read();
			sesion = request.getSession();
			sesion.setAttribute("listaArmas", res);
			response.sendRedirect("armas.jsp");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
