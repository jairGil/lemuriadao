function confirmacion(id, servlet) {

	var respuesta = confirm("¿Estás seguro de eliminar el registro?");

	if (respuesta) {

		// Redireccionar al JSP con el parámetro "id" 

		window.location.href = servlet + "?id=" + id;

	} else {

		// Acción en caso de cancelación 
		alert("Acción cancelada");
	}
}