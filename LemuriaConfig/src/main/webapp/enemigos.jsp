<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.List" %>
<%@ page import="mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Enemigo" %>
<!--
 // WEBSITE: https://themefisher.com
 // TWITTER: https://twitter.com/themefisher
 // FACEBOOK: https://www.facebook.com/themefisher
 // GITHUB: https://github.com/themefisher/
-->

<html lang="en-us">

<head>
   <meta charset="utf-8">
   <title>Adm. Enemigos</title>

   <!-- mobile responsive meta -->
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
   <meta name="description" content="This is meta description">
   <meta name="author" content="Themefisher">

   <!-- plugins -->
   <link rel="preload" href="https://fonts.gstatic.com/s/opensans/v18/mem8YaGs126MiZpBA-UFWJ0bbck.woff2" style="font-display: optional;">
   <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:600%7cOpen&#43;Sans&amp;display=swap" media="screen">

   <link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
   <link rel="stylesheet" href="plugins/slick/slick.css">

   <!-- Main Stylesheet -->
   <link rel="stylesheet" href="css/style.css">

   <!--Favicon-->
   <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
   <link rel="icon" href="images/favicon.png" type="image/x-icon">
</head>

<body>
<!-- navigation -->
<header class="sticky-top bg-white border-bottom border-default">
   <div class="container">
	<%@include file="menu_enemigos.html"%>      
   </div>
</header>
<!-- /navigation -->

<section class="section">
	<div class="container">
		<article class="row mb-4">
			<div class="col-lg-10 mx-auto mb-4">
				<h1 class="h2 mb-3">Administración de Enemigos</h1>
				<ul class="list-inline post-meta mb-3">
					<li class="list-inline-item"><i class="ti-user mr-2"></i><a href="author.html">
                     fchavez19</a>
					</li>
					<li class="list-inline-item">Fecha : Abril 26, 2023</li>
					<li class="list-inline-item">Categorias : <a href="#!" class="ml-1">Enemigos</a>
					</li>
					<li class="list-inline-item">Etqs : <a href="#!" class="ml-1">enemigo </a> ,<a href="#!" class="ml-1">admin </a>
					</li>
				</ul>
			</div>
			<div class="col-12 mb-3">
				<div class="post-slider">
					<img src="images/post/post-8.jpg" class="img-fluid" alt="post-thumb">
				</div>
			</div>
			<div class="col-lg-10 mx-auto">
				<div class="content">
                                     <h4 id="tab">Enemigos</h4>
                                        <div class="code-tabs">
                                                <ul class="nav nav-tabs"></ul>
                                                <div class="tab-content">
                                                        <div class="tab-pane" title="Consultar">
                                                          <table>
                                                             <thead>
                                                                  <tr>
                                                                    <th>Id</th>
                                                                    <th style="text-align:center">Nombre</th>
                                                                    <th style="text-align:right">Modificar</th>
                                                                    <th style="text-align:right">Borrar</th>
                                                                  </tr>
                                                             </thead>
                                                             <tbody>
                                                               <%
                                                                	List<Enemigo> lista;
                                                                	lista = (List)session.getAttribute("listaEnemigos");
                                                                	for(Enemigo e:lista){
                                                                	out.println("<tr>");
                                                                	out.println("<td>"+e.getId()+"</td>");                                                                    
                                                                	out.println("<td style=\"text-align:center\">");
                                                                	out.println("<a href=\"consultaEnemigo?id="+e.getId()+"\">"+e.getNombre()+"</a></td>");
                                                                    out.println("<td style=\"text-align:right\"><a href=\"modificaEnemigo?id="+e.getId()+"\">Actualizar</a></td>");
                                                                    out.println("<td style=\"text-align:right\"><a onclick=\"confirmacion("+e.getId()+",'eliminaEnemigo')\">Eliminar</a></td>");
                                                                    out.println("</tr>");
                                                                    }
                                                                %>
                                                             </tbody>
                                                          </table>
                                                        </div>
                                                        <div class="tab-pane" title="Altas">
                                                          <h6>Creación de Enemigos</h6>
                                                          <form action="altaEnemigos" method="post">
                                                              <p>
                                                              Nombre: <input name="enemigoNom" type="text" placeholder="Nombre del enemigo"/>
                                                              </p>
                                                              <p>
                                                              Imagen: <input type="file" name="enemigoImg"/>
                                                              </p>
                                                              <p>
                                                              Nivel de Vida: <input name="nivelVida" type="number" value="0"/>
                                                              </p>
                                                              <p>
                                                              Nivel de Ataque: <input name="nivelAtaque" type="number" value="0"/>
                                                              </p>
                                                              <p>
                                                              Nivel de defensa: <input name="nivelDefensa" type="number" value="0"/>
                                                              </p>
							      								
							      								<p>
                                                              <input type="submit" value="Enviar"/>
                                                              </p>
                                                          </form>
                                                        </div>
                                                </div>
                                        </div>
				</div>
			</div>
		</article>
	</div>
</section>
<%@include file="pie_enemigos.html"%>


   <!-- JS Plugins -->
   <script src="plugins/jQuery/jquery.min.js"></script>
   <script src="plugins/bootstrap/bootstrap.min.js" async></script>
   <script src="plugins/slick/slick.min.js"></script>
   <script src="js/mensajesDeConfirmacion.js"></script>

   <!-- Main Script -->
   <script src="js/script.js"></script>
</body>
</html>