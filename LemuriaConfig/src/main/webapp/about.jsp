<!DOCTYPE html>

<!--
 // WEBSITE: https://themefisher.com
 // TWITTER: https://twitter.com/themefisher
 // FACEBOOK: https://www.facebook.com/themefisher
 // GITHUB: https://github.com/themefisher/
-->

<html lang="en-us">

<head>
    <meta charset="utf-8">
    <title>Hi, I Am John Doe</title>

    <!-- mobile responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
    <meta name="description" content="This is meta description">
    <meta name="author" content="Themefisher">

    <!-- plugins -->
    <link rel="preload" href="https://fonts.gstatic.com/s/opensans/v18/mem8YaGs126MiZpBA-UFWJ0bbck.woff2" style="font-display: optional;">
    <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:600%7cOpen&#43;Sans&amp;display=swap" media="screen">

    <link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
    <link rel="stylesheet" href="plugins/slick/slick.css">

    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="css/style.css">

    <!--Favicon-->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">
</head>

<body>
    <!-- navigation -->
    <header class="sticky-top bg-white border-bottom border-default">
        <div class="container">
            <%@include file="menu_about.html"%>
        </div>
    </header>


    <!-- /navigation -->

    <section class="section-sm">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="title-bordered mb-5 d-flex align-items-center">
                        <h1 class="h4">Lemuria</h1>
                        <ul class="list-inline social-icons ml-auto mr-3 d-none d-sm-block">
                            <li class="list-inline-item"><a href="#"><i class="ti-facebook"></i></a>
                            </li>
                            <li class="list-inline-item"><a href="#"><i class="ti-twitter-alt"></i></a>
                            </li>
                            <li class="list-inline-item"><a href="#"><i class="ti-linkedin"></i></a>
                            </li>
                            <li class="list-inline-item"><a href="#"><i class="ti-github"></i></a>
                            </li>
                        </ul>
                    </div>
                    <img src="images/author-full.jpg" class="img-fluid w-100 mb-4 rounded-lg" alt="author">
                    <div class="content">
                        <p>Lemuria era una tierra semidesertica y por lo tanto inhabitada, salvo por unos cuantos animales salvajes. Aprovechando que era un lugar aislado y semivacío se le utilizo para exiliar a entes malvados que habitaban diferentes partes
                            del mundo. Se construyo un muro de poder que impedia a los exiliados escapar. Durante muchos siglos este lugar sirvio como una prisión muy útil, pero cuando exiliaron a la poderosa bruja Circe. Esta bruja lideró a los seres
                            que se encontraban en lemuria y transformó la isla de carcel en fortaleza. Creo diversas salas y encontró un medio para salir a voluntad de Lemuria, convenció a los exiliados de que estaban cómodos en este lugar y les asignó
                            la misión de proteger los items que robaba a lo largo y ancho del mundo. Para evitar rivalidades cada ser maligno tiene su propia sala y su propio item a proteger.
                        </p>
                        <p>
                            La humanidad estaba disgustada con la transformación de Lemuria de carcel a la fortaleza de Circe; sin embargo, soportaba la situación y preferia tratar de atraparla en sus misiones de robo porque el atacar Lemuria resultaba sumamente peligroso debido
                            al poder de la bruja y al ejercito que había formado enlistando a los exiliados. Esta situación se hubiese mantenido si Circe no hubiese robado el "Tesoro de Avalon". Objeto que le permite aumentar su poder y de sus aliados
                            combinando la magia del tesoro con pociones y energía de armas miticas. Ahora resulta impresindible entrar en Lemuria y recuperar el "Tesoro de Avalón" antes de que la bruja consiga el poder necesario para gobernar al mundo.
                        </p>
                        <div class="quote"> <i class="ti-quote-left"></i>
                            <div>
                                <p>Muevete sigilosamente a través del mapa, selecciona sabiamente tus batallas y recupera el "Tesoro de Avalón".</p>
                                <span class="quote-by"> -fchavez19</span>
                            </div>
                            <i class="ti-quote-rigth"></i>
                        </div>
                        <hr>
                        <h4 id="my-skills--experiences">Consejos:</h4>
                        <p>No podremos recuperar el "Tesoro de Avalón" sin luchar, pero debemos seleccionar adecuadamente las batallas, porque si peleamos contra todos, todo el tiempo corremos el riesgo de enfrentar las batallas más dificiles agotados o
                            con poco nivel. Por lo que para tener exito en la misión debes:</p>
                        <ul>
                            <li>Explorar</li>
                            <li>Utilzar iteligentemente los items recolectados</li>
                            <li>Luchar con pasión e inteligencia</li>
                        </ul>
                        <p>La misión ha comenzado, si sientes que estás en medio de un infierno ... continua avanzando.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    //include file="pie_about.html"%>


        <!-- JS Plugins -->
        <script src="plugins/jQuery/jquery.min.js"></script>
        <script src="plugins/bootstrap/bootstrap.min.js" async></script>
        <script src="plugins/slick/slick.min.js"></script>

        <!-- Main Script -->
        <script src="js/script.js"></script>
</body>

</html>