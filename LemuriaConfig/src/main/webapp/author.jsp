<!DOCTYPE html>

<!--
 // WEBSITE: https://themefisher.com
 // TWITTER: https://twitter.com/themefisher
 // FACEBOOK: https://www.facebook.com/themefisher
 // GITHUB: https://github.com/themefisher/
-->

<html lang="en-us">

<head>
    <meta charset="utf-8">
    <title>Autor</title>

    <!-- mobile responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
    <meta name="description" content="This is meta description">
    <meta name="author" content="Themefisher">

    <!-- plugins -->
    <link rel="preload" href="https://fonts.gstatic.com/s/opensans/v18/mem8YaGs126MiZpBA-UFWJ0bbck.woff2" style="font-display: optional;">
    <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:600%7cOpen&#43;Sans&amp;display=swap" media="screen">

    <link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
    <link rel="stylesheet" href="plugins/slick/slick.css">

    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="css/style.css">

    <!--Favicon-->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">
</head>

<body>
    <!-- navigation -->
    <header class="sticky-top bg-white border-bottom border-default">
        <div class="container">
            <%@include file="menu_about.html"%>
        </div>
    </header>
    <!-- /navigation -->

    <section class="section-sm border-bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="title-bordered mb-5 d-flex align-items-center">
                        <h1 class="h4">FI - UAEM</h1>
                        <ul class="list-inline social-icons ml-auto mr-3 d-none d-sm-block">
                            <li class="list-inline-item"><a href="#"><i class="ti-facebook"></i></a>
                            </li>
                            <li class="list-inline-item"><a href="#"><i class="ti-twitter-alt"></i></a>
                            </li>
                            <li class="list-inline-item"><a href="#"><i class="ti-github"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mb-4 mb-md-0 text-center text-md-left">
                    <img loading="lazy" class="rounded-lg img-fluid" src="images/author.jpg">
                </div>
                <div class="col-lg-9 col-md-8 content text-center text-md-left">
                    <p>
                        Los sistemas de software requieren de un proceso de trabajo largo y multifactorial que debe llevarse a cabo durante largos períodos de tiempo. La cantidad de trabajo y el tiempo de desarrollo dependerán de la complejidad del sistema, pero es posible que
                        el tiempo de desarrollo aumente aún después de haber instalado el producto, ya que los sistemas de software evolucionan con el tiempo. Es por todas estas razones que el desarrollo de software es un trabajo que se realiza en equipo.
                    </p>
                    <p>
                        Lemuria es un videojuego que tiene como objetivo llevar a los estudiantes de la facultad de Ingeniería de la UAEM a desarrollar software a través de la experiencia de mantenimiento. Es por esto que el sistema está siendo desarrollado por diferentes grupos
                        de estudiantes a lo largo del tiempo.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="section-sm">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title text-center">
                        <h2 class="mb-5">Grupos que han participado</h2>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4">
                    <article class="mb-5">
                        <div class="post-slider slider-sm">
                            <img loading="lazy" src="images/post/post-9.jpg" class="img-fluid" alt="post-thumb">
                            <img loading="lazy" src="images/post/post-10.jpg" class="img-fluid" alt="post-thumb">
                            <img loading="lazy" src="images/post/post-11.jpg" class="img-fluid" alt="post-thumb">
                            <img loading="lazy" src="images/post/post-12.jpg" class="img-fluid" alt="post-thumb">
                        </div>
                        <h3 class="h5">
                            Grupos que han contribuido al proyecto.</h3>
                        <ul class="list-inline post-meta mb-2">
                            <li class="list-inline-item"><i class="ti-user mr-2"></i><a href="author.html">
								fchavez19</a>
                            </li>
                            <li class="list-inline-item">Fecha : Abril 26, 2023</li>
                            <li class="list-inline-item">Categorias : <a href="#!" class="ml-1">Contribucion </a>
                            </li>
                            <li class="list-inline-item">Etqs : <a href="#!" class="ml-1">grupo </a> ,<a href="#!" class="ml-1">participante </a>
                            </li>
                        </ul>
                        <p>
                            Agradezco la entusiaste particpación de los alumnos en el desarrollo del presente proyecto. …</p>
                        <!--<a href="post-elements.html" class="btn btn-outline-primary">Continue Reading</a>-->
                </div>
                <div class="col-lg-4 col-sm-6 mb-4">
                    <article class="mb-5">
                        <div class="post-slider slider-sm">
                            <img loading="lazy" src="images/post/post-9.jpg" class="img-fluid" alt="post-thumb">
                        </div>
                        <h3 class="h5"><a class="post-title" href="para_I_22B.html">
							Grupo Pionero</a></h3>
                        <ul class="list-inline post-meta mb-2">
                            <li class="list-inline-item"><i class="ti-user mr-2"></i><a href="author.html">
								fchavez19</a>
                            </li>
                            <li class="list-inline-item">Fecha : Abril 26, 2023</li>
                            <li class="list-inline-item">Categorias : <a href="#!" class="ml-1">generacion </a>
                            </li>
                            <li class="list-inline-item">Etqs : <a href="#!" class="ml-1">grupo </a> ,<a href="#!" class="ml-1">participante </a>
                            </li>
                        </ul>
                        <p>Base del juego versión de escritorio sin persistencia pero con toda la mecánica de juego…</p>
                        <a href="para_I_22B.html" class="btn btn-outline-primary">Consultar</a>
                    </article>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4">
                    <article class="mb-5">
                        <div class="post-slider slider-sm">
                            <img loading="lazy" src="images/post/post-13.jpg" class="img-fluid" alt="post-thumb">
                            <img loading="lazy" src="images/post/post-14.jpg" class="img-fluid" alt="post-thumb">
                            <img loading="lazy" src="images/post/post-15.jpg" class="img-fluid" alt="post-thumb">
                        </div>
                        <h3 class="h5">
                            Tecnologías utilizadas</h3>
                        <ul class="list-inline post-meta mb-2">
                            <li class="list-inline-item"><i class="ti-user mr-2"></i><a href="author.html">
								fchavez19</a>
                            </li>
                            <li class="list-inline-item">Fecha : Abril 26, 2023</li>
                            <li class="list-inline-item">Categorias : <a href="#!" class="ml-1">Tecnologia </a>
                            </li>
                            <li class="list-inline-item">Etqs : <a href="#!" class="ml-1">Servlet </a> ,<a href="#!" class="ml-1">Derby </a>
                            </li>
                        </ul>
                        <p>Las herramientas tecnológicas potencian los sistemas.</p>
                    </article>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4">
                    <article class="mb-5">
                        <div class="post-slider slider-sm">
                            <img loading="lazy" src="images/post/post-10.jpg" class="img-fluid" alt="post-thumb">
                        </div>
                        <h3 class="h5"><a class="post-title" href="software_II_23A.html">
							Versión Web</a></h3>
                        <ul class="list-inline post-meta mb-2">
                            <li class="list-inline-item"><i class="ti-user mr-2"></i><a href="author.html">
								fchavez19</a>
                            </li>
                            <li class="list-inline-item">Fecha : Abril 26, 2023</li>
                            <li class="list-inline-item">Categorias : <a href="#!" class="ml-1">web </a>
                            </li>
                            <li class="list-inline-item">Etqs : <a href="#!" class="ml-1">web </a> ,<a href="#!" class="ml-1">version 2.0 </a>
                            </li>
                        </ul>
                        <p>Configuración para el juego y versión web</p> <a href="software_II_23A.html" class="btn btn-outline-primary">Consultar</a>
                    </article>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4">
                    <article class="mb-5">
                        <div class="post-slider slider-sm">
                            <img loading="lazy" src="images/post/post-16.jpg" class="img-fluid" alt="post-thumb">
                            <img loading="lazy" src="images/post/post-17.jpg" class="img-fluid" alt="post-thumb">
                            <img loading="lazy" src="images/post/post-18.jpg" class="img-fluid" alt="post-thumb">
                            <img loading="lazy" src="images/post/post-19.jpg" class="img-fluid" alt="post-thumb">
                            <img loading="lazy" src="images/post/post-20.jpg" class="img-fluid" alt="post-thumb">
                            <img loading="lazy" src="images/post/post-21.jpg" class="img-fluid" alt="post-thumb">
                            <img loading="lazy" src="images/post/post-22.jpg" class="img-fluid" alt="post-thumb">
                            <img loading="lazy" src="images/post/post-23.jpg" class="img-fluid" alt="post-thumb">
                        </div>
                        <h3 class="h5">Herramientas y patrones
                        </h3>
                        <ul class="list-inline post-meta mb-2">
                            <li class="list-inline-item"><i class="ti-user mr-2"></i><a href="author.html">
								fchavez19</a>
                            </li>
                            <li class="list-inline-item">Fecha : Abril 26, 2023</li>
                            <li class="list-inline-item">Categorias : <a href="#!" class="ml-1">Herramientas </a>
                            </li>
                            <li class="list-inline-item">Etqs : <a href="#!" class="ml-1">Herramientas </a> ,<a href="#!" class="ml-1">Patrones </a>
                            </li>
                        </ul>
                        <p>Es importante utilizar patrones de diseño y herramientas de desarrollo adecuadas.</p>
                        <!--<a href="post-details-1.html" class="btn btn-outline-primary">Continue Reading</a>-->
                    </article>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4">
                    <article class="mb-5">
                        <div class="post-slider slider-sm">
                            <img loading="lazy" src="images/post/post-11.jpg" class="img-fluid" alt="post-thumb">
                        </div>
                        <h3 class="h5"><a class="post-title" href="diseno_23A.html">
							Versión Web</a></h3>
                        <ul class="list-inline post-meta mb-2">
                            <li class="list-inline-item"><i class="ti-user mr-2"></i><a href="author.html">
								fchavez19</a>
                            </li>
                            <li class="list-inline-item">Fecha : Abril 26, 2023</li>
                            <li class="list-inline-item">Categorias : <a href="#!" class="ml-1">Web </a>
                            </li>
                            <li class="list-inline-item">Tags : <a href="#!" class="ml-1">web </a> ,<a href="#!" class="ml-1">Version 2.0 </a>
                            </li>
                        </ul>
                        <p>Configuración para el juego y versión web.</p> <a href="diseno_23A.html" class="btn btn-outline-primary">Consultar</a>
                    </article>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4">
                    <article class="mb-5">
                        <div class="post-slider slider-sm">
                            <img loading="lazy" src="images/post/post-12.jpg" class="img-fluid" alt="post-thumb">
                        </div>
                        <h3 class="h5"><a class="post-title" href="para_I_23A.html">
							Admninistración versión de escritorio</a></h3>
                        <ul class="list-inline post-meta mb-2">
                            <li class="list-inline-item"><i class="ti-user mr-2"></i><a href="author.html">
								fchavez19</a>
                            </li>
                            <li class="list-inline-item">Fecha : Abril 26, 2023</li>
                            <li class="list-inline-item">Categorias : <a href="#!" class="ml-1">generación </a>
                            </li>
                            <li class="list-inline-item">Etqs : <a href="#!" class="ml-1">grupo </a> ,<a href="#!" class="ml-1">participante </a>
                            </li>
                        </ul>
                        <p>Sistena versión escritorio para la administración del juego.</p> <a href="para_I_23A.html" class="btn btn-outline-primary">Consultar </a>
                    </article>
                </div>
            </div>
        </div>
    </section>

   //include file="pie_author.html"

        <!-- JS Plugins -->
        <script src="plugins/jQuery/jquery.min.js"></script>
        <script src="plugins/bootstrap/bootstrap.min.js" async></script>
        <script src="plugins/slick/slick.min.js"></script>

        <!-- Main Script -->
        <script src="js/script.js"></script>
</body>

</html>