<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.List" %>
<%@ page import="mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Pocion" %>
<!--
 // WEBSITE: https://themefisher.com
 // TWITTER: https://twitter.com/themefisher
 // FACEBOOK: https://www.facebook.com/themefisher
 // GITHUB: https://github.com/themefisher/
-->

<html lang="en-us">

<head>
   <meta charset="utf-8">
   <title>Adm. Pociones</title>

   <!-- mobile responsive meta -->
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
   <meta name="description" content="This is meta description">
   <meta name="author" content="Themefisher">

   <!-- plugins -->
   <link rel="preload" href="https://fonts.gstatic.com/s/opensans/v18/mem8YaGs126MiZpBA-UFWJ0bbck.woff2" style="font-display: optional;">
   <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:600%7cOpen&#43;Sans&amp;display=swap" media="screen">

   <link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
   <link rel="stylesheet" href="plugins/slick/slick.css">

   <!-- Main Stylesheet -->
   <link rel="stylesheet" href="css/style.css">

   <!--Favicon-->
   <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
   <link rel="icon" href="images/favicon.png" type="image/x-icon">
</head>

<body>
<!-- navigation -->
<header class="sticky-top bg-white border-bottom border-default">
   <div class="container">
	<%@include file="menu_pociones.html"%>      
   </div>
</header>
<!-- /navigation -->

<section class="section">
	<div class="container">
		<article class="row mb-4">
			<div class="col-lg-10 mx-auto mb-4">
				<h1 class="h2 mb-3">Modificación de Pociones</h1>
				<ul class="list-inline post-meta mb-3">
					<li class="list-inline-item"><i class="ti-user mr-2"></i><a href="author.html">
                     fchavez19</a>
					</li>
					<li class="list-inline-item">Fecha : Abril 26, 2023</li>
					<li class="list-inline-item">Categorias : <a href="#!" class="ml-1">Pociones</a>
					</li>
					<li class="list-inline-item">Etqs : <a href="#!" class="ml-1">pocion </a> ,<a href="#!" class="ml-1">update </a>
					</li>
				</ul>
			</div>
			<div class="col-12 mb-3">
				<div class="post-slider">
					<img src="images/post/post-6.jpg" class="img-fluid" alt="post-thumb">
				</div>
			</div>
			<div class="col-lg-10 mx-auto">
				<div class="content">
                                     <h4 id="tab">Modificar pocion</h4>
                                        <div class="code-tabs">
                                                <ul class="nav nav-tabs"></ul>
                                                <div class="tab-content">
                                                        <div class="tab-pane" title="Modificar">
															<form action="modificaPocion" method="post">
                                                              <%
                                                                	Pocion p;
                                                                	p = (Pocion)session.getAttribute("Pocion");
                                                                	
                                                                	out.println("<p>");
                                                                	out.println("Nombre: <input name=\"Nom\" type=\"text\" value=\""+p.getNombre()+"\"/>");                                                                    
                                                                	out.println("</p>");
                                                                	out.println("<p>");
                                                                	out.println("Imagen: <input type=\"file\" name=\"Img\" value=\""+p.getImagen()+"\"/>");                                                                    
                                                                	out.println("</p>");
                                                                	out.println("<p>");
                                                                	out.println("Nivel: <input name=\"nivel\" type=\"number\" value=\""+p.getNivel()+"\"/>");                                                                    
                                                                	out.println("</p>");
                                                                	out.println("<p>");
                                                                	out.println("Tipo: <input name=\"tipo\" type=\"text\" value=\""+p.getTipo()+"\"/>");                                                                    
                                                                	out.println("</p>");
                                                                	
                                                                	
                               
                                                                %>
							      <p>
                                                              <input type="submit" value="Enviar"/>
                                                              </p>
                                                              
                                                          </form>
                                                        </div>
                             
                                                          
                                                        </div>
       
                                                              
                                                         
                                                       
                                                </div>
                                        </div>
				</div>
			</div>
		</article>
	</div>
</section>
<%@include file="pie_pociones.html"%>


   <!-- JS Plugins -->
   <script src="plugins/jQuery/jquery.min.js"></script>
   <script src="plugins/bootstrap/bootstrap.min.js" async></script>
   <script src="plugins/slick/slick.min.js"></script>

   <!-- Main Script -->
   <script src="js/script.js"></script>
</body>
</html>