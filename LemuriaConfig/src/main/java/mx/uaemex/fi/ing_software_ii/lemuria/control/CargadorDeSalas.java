package mx.uaemex.fi.ing_software_ii.lemuria.control;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.ArmasDAO;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby.ArmasDAODerbyImp;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby.SalaDAODerbyImp;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Arma;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Sala;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Servlet implementation class CargadorDeSalas
 */
public class CargadorDeSalas extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private SalaDAODerbyImp dao;
	private DataSource ds;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		
		InitialContext cxt;
		
		try {
			cxt=new InitialContext();
			if(cxt!=null) {
				this.ds=(DataSource)cxt.lookup("java:/comp/env/jdbc/ds");
			}
			if(this.ds==null) {
				throw new ServletException("DataSource no econtrado");
			}
		}catch(NamingException e) {
			throw new ServletException("Sin contexto inicial");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SalaDAODerbyImp realDao;
		Connection con;
		List<Sala> res;
		HttpSession sesion;
		
		realDao = new SalaDAODerbyImp();
		
		try {
			con = this.ds.getConnection();
			realDao.setConexion(con);
			this.dao = realDao;
			res = this.dao.read();
			sesion = request.getSession();
			sesion.setAttribute("listaSalas", res);
			response.sendRedirect("salas.jsp");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
