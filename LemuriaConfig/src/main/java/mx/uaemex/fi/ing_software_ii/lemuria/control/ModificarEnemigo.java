package mx.uaemex.fi.ing_software_ii.lemuria.control;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.EnemigosDAO;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby.EnemigosDAODerbyImp;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Enemigo;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Servlet implementation class ModificarEnemigo
 */
public class ModificarEnemigo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private EnemigosDAO dao;
	private DataSource ds;
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		InitialContext cxt;

		try {
			cxt = new InitialContext();
			if (cxt != null) {
				this.ds = (DataSource) cxt.lookup("java:/comp/env/jdbc/ds");
			}
			if (this.ds == null) {
				throw new ServletException("DataSource no econtrado");
			}
		} catch (NamingException e) {
			throw new ServletException("Sin contexto inicial");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EnemigosDAODerbyImp realDao;
		Connection con;
		Enemigo e1 = new Enemigo();
		
		HttpSession sesion;
		String id;
		
		realDao = new EnemigosDAODerbyImp();
		
		try {
			con = this.ds.getConnection();
			realDao.setConexion(con);
			this.dao= realDao;
			
			id = request.getParameter("id");
			e1.setId(Integer.parseInt(id));
			e1 = this.dao.read(e1);
			sesion = request.getSession();
			sesion.setAttribute("Enemigo", e1);
			
			response.sendRedirect("update_enemigo.jsp");
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EnemigosDAODerbyImp realDao;
		Connection con;
		HttpSession sesion;
		Enemigo e1 = new Enemigo();
		Enemigo e2 = new Enemigo();
		
		
		
		realDao = new EnemigosDAODerbyImp();
		try {
			String nombre = request.getParameter("armaNom");
			String img = request.getParameter("armaImg");
			int vida = Integer.parseInt(request.getParameter("vida"));
			int ataque = Integer.parseInt(request.getParameter("ataque"));
			int defensa = Integer.parseInt(request.getParameter("defensa"));
			
			con = this.ds.getConnection();
			realDao.setConexion(con);
			this.dao= realDao;
			sesion = request.getSession();
			
			e1.setNombre(nombre);
			e1.setImagen(img);
			e1.setVida(vida);
			e1.setAtaque(ataque);
			e1.setDefensa(defensa);
			
			e2 = (Enemigo)sesion.getAttribute("Enemigo");
			
			this.dao.update(e2, e1);
			e1 = this.dao.read(e2);
			sesion.setAttribute("Enemigo", e1);
			response.sendRedirect("cargaEnemigos");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
