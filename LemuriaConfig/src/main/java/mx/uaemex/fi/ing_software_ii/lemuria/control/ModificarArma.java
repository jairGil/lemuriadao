package mx.uaemex.fi.ing_software_ii.lemuria.control;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.ArmasDAO;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby.ArmasDAODerbyImp;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Arma;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Servlet implementation class ModificarArma
 */
public class ModificarArma extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ArmasDAO dao;
	private DataSource ds;
	

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		
		InitialContext cxt;
		
		try {
			cxt=new InitialContext();
			if(cxt!=null) {
				this.ds=(DataSource)cxt.lookup("java:/comp/env/jdbc/ds");
			}
			if(this.ds==null) {
				throw new ServletException("DataSource no econtrado");
			}
		}catch(NamingException e) {
			throw new ServletException("Sin contexto inicial");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArmasDAODerbyImp realDao;
		Connection con;
		Arma a1 = new Arma();
		
		HttpSession sesion;
		String id;
		
		realDao = new ArmasDAODerbyImp();
		
		try {
			con = this.ds.getConnection();
			realDao.setConexion(con);
			this.dao= realDao;
			
			id = request.getParameter("id");
			a1.setId(Integer.parseInt(id));
			a1 = this.dao.read(a1);
			sesion = request.getSession();
			sesion.setAttribute("Arma", a1);
			
			response.sendRedirect("update_arma.jsp");
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArmasDAODerbyImp realDao;
		Connection con;
		HttpSession sesion;
		Arma a1 = new Arma();
		Arma a2 = new Arma();
		
		
		
		realDao = new ArmasDAODerbyImp();
		try {
			String nombre = request.getParameter("armaNom");
			String img = request.getParameter("armaImg");
			int nivel = Integer.parseInt(request.getParameter("nivel"));
			String des = request.getParameter("descripcion");
			
			con = this.ds.getConnection();
			realDao.setConexion(con);
			this.dao= realDao;
			sesion = request.getSession();
			
			a1.setNombre(nombre);
			a1.setImagen(img);
			a1.setNivel(nivel);
			a1.setDescripcion(des);
			
			a2 = (Arma)sesion.getAttribute("Arma");
			
			this.dao.update(a2, a1);
			a1 = this.dao.read(a2);
			sesion.setAttribute("Arma", a1);
			response.sendRedirect("cargaArmas");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
