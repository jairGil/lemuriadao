package mx.uaemex.fi.ing_software_ii.lemuria.control;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.ArmasDAO;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby.ArmasDAODerbyImp;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Arma;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Servlet implementation class AltaDeArmas
 */
public class AltaDeArmas extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ArmasDAO dao;
	private DataSource ds;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		
		InitialContext cxt;
		
		try {
			cxt=new InitialContext();
			if(cxt!=null) {
				this.ds=(DataSource)cxt.lookup("java:/comp/env/jdbc/ds");
			}
			if(this.ds==null) {
				throw new ServletException("DataSource no econtrado");
			}
		}catch(NamingException e) {
			throw new ServletException("Sin contexto inicial");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArmasDAODerbyImp realDao;
		Connection con;
		Arma a = new Arma();
		
		realDao = new ArmasDAODerbyImp();
		
		try {
			String nombre = request.getParameter("armaNom");
			String img = request.getParameter("armaImg");
			int nivel = Integer.parseInt(request.getParameter("nivel"));
			String descripcion = request.getParameter("descripcion");
			
			con = this.ds.getConnection();
			realDao.setConexion(con);
			this.dao = realDao;
			a.setNombre(nombre);
			a.setNivel(nivel);
			a.setImagen(img);
			a.setDescripcion(descripcion);
			this.dao.create(a);
			response.sendRedirect("cargaArmas");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
