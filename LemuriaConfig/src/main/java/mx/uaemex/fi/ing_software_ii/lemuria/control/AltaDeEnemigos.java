package mx.uaemex.fi.ing_software_ii.lemuria.control;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.EnemigosDAO;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby.EnemigosDAODerbyImp;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Enemigo;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Servlet implementation class AltaDeEnemigos
 */
public class AltaDeEnemigos extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private EnemigosDAO dao;
	private DataSource ds;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		
		InitialContext cxt;
		
		try {
			cxt=new InitialContext();
			if(cxt!=null) {
				this.ds=(DataSource)cxt.lookup("java:/comp/env/jdbc/ds");
			}
			if(this.ds==null) {
				throw new ServletException("DataSource no econtrado");
			}
		}catch(NamingException e) {
			throw new ServletException("Sin contexto inicial");
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EnemigosDAODerbyImp realDao;
		Connection con;
		Enemigo e = new Enemigo();
		
		realDao = new EnemigosDAODerbyImp();
		try {
			String nombre = request.getParameter("enemigoNom");
			String img = request.getParameter("enemigoImg");
			int vida = Integer.parseInt(request.getParameter("nivelVida"));
			int ataque = Integer.parseInt(request.getParameter("nivelAtaque"));
			int defensa = Integer.parseInt(request.getParameter("nivelDefensa"));
			
			con = this.ds.getConnection();
			realDao.setConexion(con);
			this.dao = realDao;
			
			e.setNombre(nombre);
			e.setVida(vida);
			e.setAtaque(ataque);
			e.setDefensa(defensa);
			e.setImagen(img);
			this.dao.create(e);
			response.sendRedirect("cargaEnemigos");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
