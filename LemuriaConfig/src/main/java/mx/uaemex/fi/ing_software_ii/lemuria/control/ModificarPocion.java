package mx.uaemex.fi.ing_software_ii.lemuria.control;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.PocionesDAO;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby.PocionesDAODerbyImp;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Pocion;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Servlet implementation class ModificarPocion
 */
public class ModificarPocion extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PocionesDAO dao;
	private DataSource ds;
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		InitialContext cxt;

		try {
			cxt = new InitialContext();
			if (cxt != null) {
				this.ds = (DataSource) cxt.lookup("java:/comp/env/jdbc/ds");
			}
			if (this.ds == null) {
				throw new ServletException("DataSource no econtrado");
			}
		} catch (NamingException e) {
			throw new ServletException("Sin contexto inicial");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PocionesDAODerbyImp realDao;
		Connection con;
		Pocion p1 = new Pocion();
		
		HttpSession sesion;
		String id;
		
		realDao = new PocionesDAODerbyImp();
		
		try {
			con = this.ds.getConnection();
			realDao.setConexion(con);
			this.dao= realDao;
			
			id = request.getParameter("id");
			p1.setId(Integer.parseInt(id));
			p1 = this.dao.read(p1);
			sesion = request.getSession();
			sesion.setAttribute("Pocion", p1);
			
			response.sendRedirect("update_pocion.jsp");
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PocionesDAODerbyImp realDao;
		Connection con;
		HttpSession sesion;
		Pocion p1 = new Pocion();
		Pocion p2 = new Pocion();
		
		
		
		realDao = new PocionesDAODerbyImp();
		try {
			String nombre = request.getParameter("Nom");
			String img = request.getParameter("Img");
			int nivel = Integer.parseInt(request.getParameter("nivel"));
			int tipo = Integer.parseInt(request.getParameter("tipo"));
			
			con = this.ds.getConnection();
			realDao.setConexion(con);
			this.dao= realDao;
			sesion = request.getSession();
			
			p1.setNombre(nombre);
			p1.setImagen(img);
			p1.setNivel(nivel);
			p1.setTipo(tipo);
			
			p2 = (Pocion)sesion.getAttribute("Pocion");
			
			this.dao.update(p2, p1);
			p1 = this.dao.read(p2);
			sesion.setAttribute("Pocion", p1);
			response.sendRedirect("cargaPociones");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
