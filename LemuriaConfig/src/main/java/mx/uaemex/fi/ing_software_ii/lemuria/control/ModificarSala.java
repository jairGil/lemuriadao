package mx.uaemex.fi.ing_software_ii.lemuria.control;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.SalaDAO;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby.SalaDAODerbyImp;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Sala;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Servlet implementation class ModificarSala
 */
public class ModificarSala extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private SalaDAO dao;
	private DataSource ds;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 * 
	 */
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		
		InitialContext cxt;
		
		try {
			cxt=new InitialContext();
			if(cxt!=null) {
				this.ds=(DataSource)cxt.lookup("java:/comp/env/jdbc/ds");
			}
			if(this.ds==null) {
				throw new ServletException("DataSource no econtrado");
			}
		}catch(NamingException e) {
			throw new ServletException("Sin contexto inicial");
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SalaDAODerbyImp realDao;
		Connection con;
		Sala s1 = new Sala();
		
		HttpSession sesion;
		String id;
		
		realDao = new SalaDAODerbyImp();
		
		try {
			con = this.ds.getConnection();
			realDao.setConexion(con);
			this.dao= realDao;
			
			id = request.getParameter("id");
			s1.setId(Integer.parseInt(id));
			s1 = this.dao.read(s1);
			sesion = request.getSession();
			sesion.setAttribute("Sala", s1);
			
			response.sendRedirect("update_sala.jsp");
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SalaDAODerbyImp realDao;
		Connection con;
		HttpSession sesion;
		Sala s1 = new Sala();
		Sala s2 = new Sala();
		
		
		
		realDao = new SalaDAODerbyImp();
		try {
			String nombre = request.getParameter("armaNom");
			String img = request.getParameter("armaImg");
			
			con = this.ds.getConnection();
			realDao.setConexion(con);
			this.dao= realDao;
			sesion = request.getSession();
			
			s1.setNombre(nombre);
			s1.setImagen(img);
		
			
			s2 = (Sala)sesion.getAttribute("Sala");
			
			this.dao.update(s2, s1);
			s1 = this.dao.read(s2);
			sesion.setAttribute("Arma", s1);
			response.sendRedirect("cargaSalas");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

}
