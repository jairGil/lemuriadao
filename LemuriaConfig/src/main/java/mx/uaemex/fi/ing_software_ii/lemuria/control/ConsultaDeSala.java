package mx.uaemex.fi.ing_software_ii.lemuria.control;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.SalaDAO;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby.SalaDAODerbyImp;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Sala;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Servlet implementation class ConsultaDeSala
 */
public class ConsultaDeSala extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private SalaDAO dao;
	private DataSource ds;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		InitialContext cxt;

		try {
			cxt = new InitialContext();
			if (cxt != null) {
				this.ds = (DataSource) cxt.lookup("java:/comp/env/jdbc/ds");
			}
			if (this.ds == null) {
				throw new ServletException("DataSource no econtrado");
			}
		} catch (NamingException e) {
			throw new ServletException("Sin contexto inicial");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SalaDAODerbyImp realDao;
		Connection con;
		HttpSession sesion;
		Sala s1 = new Sala();
		String id;
		
		
		realDao = new SalaDAODerbyImp();
		try {
			con = this.ds.getConnection();
			realDao.setConexion(con);
			this.dao= realDao;
			id = request.getParameter("id");
			s1.setId(Integer.parseInt(id));
			s1 = this.dao.read(s1);
			sesion = request.getSession();
			sesion.setAttribute("Sala", s1);
			response.sendRedirect("ConsultaSala.jsp");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



}
