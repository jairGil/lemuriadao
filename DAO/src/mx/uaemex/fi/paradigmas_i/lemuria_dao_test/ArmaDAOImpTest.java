package mx.uaemex.fi.paradigmas_i.lemuria_dao_test;

import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby.ArmasDAODerbyImp;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Arma;

public class ArmaDAOImpTest {
	public static void main(String args[]) {
		Conexion conexion = new Conexion();
		Arma arma = new Arma();
		Arma armaUpdate = new Arma();
		
		conexion.setConexion();
		ArmasDAODerbyImp implArmas = new ArmasDAODerbyImp(conexion.getConnection());

		arma.setNombre("Arma1");
		arma.setNivel(1);
		arma.setImagen("img.jpg");
		arma.setDescripcion("Test");
		implArmas.create(arma);
		arma = implArmas.read(arma);
		imprimeArma(arma);
		
		armaUpdate.setDescripcion("Test de cambio");
		armaUpdate.setNivel(3);
		armaUpdate.setImagen("otra.jpg");
		implArmas.update(arma, armaUpdate);
		arma = implArmas.read(armaUpdate);
		imprimeArma(arma);
		
		implArmas.delete(arma);

		conexion.closeConexion();
	}
	
	public static void imprimeArma(Arma a) {
		System.out.println(String.format("++++++++++++++++++++++++++++++++++++++\n"
				+ "ID: %d\n"
				+ "Nombre: %s\n"
				+ "Nivel: %s\n"
				+ "Imagen: %s\n"
				+ "Descripcion: %s\n"
				+ "++++++++++++++++++++++++++++++++++++++", 
				a.getId(), a.getNombre(), a.getNivel(), a.getImagen(), a.getDescripcion()));
	}
}
