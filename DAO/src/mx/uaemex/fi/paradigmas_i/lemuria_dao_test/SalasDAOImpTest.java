package mx.uaemex.fi.paradigmas_i.lemuria_dao_test;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby.SalaDAODerbyImp;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.*;


public class SalasDAOImpTest {
public static void main(String args[]) {
		Conexion conexion = new Conexion();
		Sala sala = new Sala();
		Sala salaUpdate = new Sala();
		Item objeto = new Arma();
        Enemigo trump = new Enemigo();


		conexion.setConexion();
		SalaDAODerbyImp implSalas = new SalaDAODerbyImp(conexion.getConnection());
        
		sala.setNombre("Sala");
        sala.setCoso(objeto);
        sala.setImagen("imagen.jpg");
        sala.setMasSiOsare(trump);
		implSalas.create(sala);
		sala = implSalas.read(sala);
		imprimeSalas(sala);
		
		salaUpdate.setCoso(objeto);
        salaUpdate.setMasSiOsare(trump);
		implSalas.update(sala, salaUpdate);
		sala = implSalas.read(salaUpdate);
		imprimeSalas(sala);
		
		implSalas.delete(sala);

		conexion.closeConexion();
	}
	
	public static void imprimeSalas(Sala s) {
		System.out.println(String.format("++++++++++++++++++++++++++++++++++++++\n"
				+ "ID: %d\n"
				+ "Nombre: %s\n"
				+ "Coso: %s\n"
				+ "Imagen: %s\n"
				+ "Massiosare: %s\n"
				+ "++++++++++++++++++++++++++++++++++++++", 
				s.getId(), s.getNombre(), s.getCoso(), s.getImagen(), s.getMasSiOsare()));
	}
    
}
