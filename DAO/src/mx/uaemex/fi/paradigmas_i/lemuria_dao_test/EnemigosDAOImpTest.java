package mx.uaemex.fi.paradigmas_i.lemuria_dao_test;

import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby.EnemigosDAODerbyImp;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Enemigo;

public class EnemigosDAOImpTest {

    public static void main(String[] args) {
        Conexion conexion = new Conexion();
		Enemigo enemigo = new Enemigo();
		Enemigo enemigoUpdate = new Enemigo();
		
		conexion.setConexion();
		EnemigosDAODerbyImp implEnemigo = new EnemigosDAODerbyImp(conexion.getConnection());

		enemigo.setNombre("Enemigo 1");
        enemigo.setAtaque(200);
        enemigo.setDefensa(100);
        enemigo.setVida(100);
		enemigo.setImagen("img.jpg");
		implEnemigo.create(enemigo);
		enemigo = implEnemigo.read(enemigo);
		imprimeEnemigo(enemigo);
		
		enemigoUpdate.setNombre("Test de cambio");
		enemigoUpdate.setImagen("otra.jpg");
		implEnemigo.update(enemigo, enemigoUpdate);
		enemigo = implEnemigo.read(enemigoUpdate);
		imprimeEnemigo(enemigo);
		
		implEnemigo.delete(enemigo);

		conexion.closeConexion();
	}
	
	public static void imprimeEnemigo(Enemigo e) {
		System.out.println(String.format("++++++++++++++++++++++++++++++++++++++\n"
				+ "ID: %d\n"
				+ "Nombre: %s\n"
				+ "Vida: %s\n"
				+ "Ataque: %s\n"
				+ "Defensa: %s\n"
				+ "Imagen: %s\n"
				+ "++++++++++++++++++++++++++++++++++++++", 
				e.getId(), e.getNombre(), e.getVida(), e.getAtaque(), e.getDefensa(), e.getImagen()));
	}
}
