package mx.uaemex.fi.paradigmas_i.lemuria_dao_test;

import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby.PocionesDAODerbyImp;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Pocion;

public class PocionesDAOImpTest {
    public static void main(String[] args) {
    Conexion conexion = new Conexion();
		Pocion pocion = new Pocion();
		Pocion pocionUpdate = new Pocion();
		
		conexion.setConexion();
		PocionesDAODerbyImp implPociones = new PocionesDAODerbyImp(conexion.getConnection());

		pocion.setNombre("Arma1");
		pocion.setNivel(1);
		pocion.setImagen("img.jpg");
		pocion.setDescripcion("Test");
        pocion.setTipo(2);
		implPociones.create(pocion);
		pocion = implPociones.read(pocion);
        imprimePocion(pocion);
		
		pocionUpdate.setDescripcion("Test de cambio");
		pocionUpdate.setNivel(3);
		pocionUpdate.setImagen("otra.jpg");
        pocionUpdate.setTipo(1);
		implPociones.update(pocion, pocionUpdate);
		pocion = implPociones.read(pocionUpdate);
        
		imprimePocion(pocion);
		
		implPociones.delete(pocion);

		conexion.closeConexion();
	}
	
	public static void imprimePocion(Pocion a) {
		System.out.println(String.format("++++++++++++++++++++++++++++++++++++++\n"
				+ "ID: %d\n"
				+ "Nombre: %s\n"
				+ "Nivel: %s\n"
				+ "Imagen: %s\n"
				+ "Tipo: %s\n"
				+ "++++++++++++++++++++++++++++++++++++++", 
				a.getId(), a.getNombre(), a.getNivel(), a.getImagen(), a.getTipo()));    
    }
    
}
