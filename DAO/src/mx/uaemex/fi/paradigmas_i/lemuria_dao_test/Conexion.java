package mx.uaemex.fi.paradigmas_i.lemuria_dao_test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
    String driver = "org.apache.derby.jdbc.EmbeddedDriver";
	String jdbcURL = "jdbc:derby:prueba;create=true";
    Connection connection;

    public void setConexion() {
        try {
            Class.forName(this.driver);
            this.connection = DriverManager.getConnection(jdbcURL);
            System.out.println("Conectado a la base de datos");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
        	e.printStackTrace();
        }
    }
    
    public void closeConexion() {
        try {
            this.connection.close();
            System.out.println("Conexion cerrada correctamente");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public Connection getConnection() {
    	return this.connection;
    }
}
