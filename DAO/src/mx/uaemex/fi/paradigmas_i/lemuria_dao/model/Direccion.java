package mx.uaemex.fi.paradigmas_i.lemuria_dao.model;
/*Christian Gabriel Gutierrez Gonzalez
 * Erick Mendieta Rodríguez
 * Emanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores*/
public class Direccion {
	public static final int ARRIBA = 1;
	public static final int ABAJO = 2;
	public static final int DERECHA = 3;
	public static final int IZQUIERDA = 4;
}
