package mx.uaemex.fi.paradigmas_i.lemuria_dao.model;
/*Christian Gabriel Gutierrez Gonzalez
 * Erick Mendieta Rodríguez
 * Emanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores*/
import mx.uaemex.fi.paradigmas_i.lemuria_dao.error.Sala404Exception;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Sala;

public class Mapa {
	private int columnas;
	private int renglones;
	private Sala[][] cuadrantes;
	private Coordenada posicionActual;
	
	
	public Mapa(int c, int r) {
		super();
		this.columnas = c;
		this.renglones = r;
	}


	public Mapa(Sala[][] salas) {
		super();
		this.cuadrantes = salas;
	}


	public Sala getSala() {
		Sala s = cuadrantes[posicionActual.getX()][posicionActual.getY()];
        if(s==null) {
            throw new Sala404Exception("El juego no esta completamente configurado, existen salas sin definir");
        }
        return s;
	}


	public void setSala(Sala s, Coordenada c) {
		this.posicionActual = c;
	}
	
	public void cambiarSala(int dir) {
		
	}
	

}
