package mx.uaemex.fi.paradigmas_i.lemuria_dao.model;
/*Christian Gabriel Gutierrez Gonzalez
 * Erick Mendieta Rodríguez
 * Emanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores*/
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Arma;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Guerrero;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Pocion;

public class Personaje extends Guerrero {
	private Mochila mochila;
	private Arma mano;
	private int maxAtaque;
	private int maxDefensa;
	private int maxVida;
	
	
	public Personaje(int mD, int mA, int mV) {
		super();
		
		this.maxDefensa = mD;
		this.maxAtaque = mA;
		this.maxVida = mV;
	}
	public void usarPocion(Pocion p) {
	}
	public Mochila getMochila() {
		return mochila;
	}
	public void setMochila(Mochila mochila) {
		this.mochila = mochila;
	}
	public Arma getArmaEnMano() {
		return mano;
	}
	public void setArmaEnMano(Arma a) {
		this.mano = a;
	}
	

}
