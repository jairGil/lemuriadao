/*
 * Christian Gabriel Gutièrrez Gonzàlez
 * Erick Mendieta Rodriguez
 * Emmanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Gair Gil Dolores
 */
package mx.uaemex.fi.paradigmas_i.lemuria_dao.error;

public class Sala404Exception extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Sala404Exception() {
		// TODO Auto-generated constructor stub
	}

	public Sala404Exception(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public Sala404Exception(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public Sala404Exception(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public Sala404Exception(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
