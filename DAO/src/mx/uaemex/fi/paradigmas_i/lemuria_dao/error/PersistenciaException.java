/*
 * Christian Gabriel Gutièrrez Gonzàlez
 * Erick Mendieta Rodriguez
 * Emmanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Gair Gil Dolores
 */

package mx.uaemex.fi.paradigmas_i.lemuria_dao.error;

public class PersistenciaException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PersistenciaException() {
		super();
	}

	public PersistenciaException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PersistenciaException(String message, Throwable cause) {
		super(message, cause);
	}

	public PersistenciaException(String message) {
		super(message);
	}

	public PersistenciaException(Throwable cause) {
		super(cause);
	}

}
