package mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs;
/*Christian Gabriel Gutierrez Gonzalez
 * Erick Mendieta Rodríguez
 * Emanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores*/
public class Pocion extends Item{
	private int tipo;
	
	

	public Pocion() {
		super();
	}



	public int getTipo() {
		return tipo;
	}



	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	

}
