package mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs;

/*Christian Gabriel Gutierrez Gonzalez
 * Erick Mendieta Rodríguez
 * Emanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores*/

public class TiposPocion {
	public static final int VIDA = 1;
	public static final int PODER = 2;
	public static final int DEFENSA = 3;
}
