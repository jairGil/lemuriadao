package mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs;
/*Christian Gabriel Gutierrez Gonzalez
 * Erick Mendieta Rodríguez
 * Emanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores*/
public class Guerrero extends ElementoConNombre {
	private int vida;
	private int defensa;
	private int ataque;
	
	public Guerrero() {
		
	}

	public int getVida() {
		return vida;
	}

	public void setVida(int vida) {
		this.vida = vida;
	}

	public int getDefensa() {
		return defensa;
	}

	public void setDefensa(int defensa) {
		this.defensa = defensa;
	}

	public int getAtaque() {
		return ataque;
	}

	public void setAtaque(int ataque) {
		this.ataque = ataque;
	}
	

}
