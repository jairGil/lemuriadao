package mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs;
/*Christian Gabriel Gutierrez Gonzalez
 * Erick Mendieta Rodríguez
 * Emanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores*/
import java.util.Objects;

public abstract class Item extends ElementoConNombre {
	 protected int nivel;
	 protected String descripcion;
	
	public Item() {
		
	}
	public int getNivel() {
		return nivel;
	}
	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		return Objects.equals(descripcion, other.descripcion) && nivel == other.nivel;
	}
	
	
	 
	 
	 
}
