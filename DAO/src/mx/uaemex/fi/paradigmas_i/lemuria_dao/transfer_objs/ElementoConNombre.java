package mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs;
/*Christian Gabriel Gutierrez Gonzalez
 * Erick Mendieta Rodríguez
 * Emanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores*/
import java.util.Objects;

public abstract class ElementoConNombre {
	protected String nombre;
	protected String imagen;
	protected int id;
	
	
	public ElementoConNombre(){
		
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "ElementoConNombre [nombre=" + nombre + ", imagen=" + imagen + ", id=" + id + "]";
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ElementoConNombre other = (ElementoConNombre) obj;
		return id == other.id && Objects.equals(imagen, other.imagen) && Objects.equals(nombre, other.nombre);
	}
	
}
