package mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs;
/*Christian Gabriel Gutierrez Gonzalez
 * Erick Mendieta Rodríguez
 * Emanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores*/
public class Sala extends ElementoConNombre {
	private Enemigo masSiOsare;
	private Item coso;
	
	public Sala() {
		
	}
	public Sala(String n) {
		
	}
	
	public Sala(String n, Enemigo e, Item c) {
		super();
		this.masSiOsare = e;
		this.coso = c;
	}
	public Enemigo getMasSiOsare() {
		return masSiOsare;
	}
	public void setMasSiOsare(Enemigo masSiOsare) {
		this.masSiOsare = masSiOsare;
	}
	public Item getCoso() {
		return coso;
	}
	public void setCoso(Item coso) {
		this.coso = coso;
	}
	@Override
	public String toString() {
		return "Sala [masSiOsare=" + masSiOsare + ", coso=" + coso + "]";
	}
	
	
}
