package mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.AbstractDAO;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.SalaDAO;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.error.ElementoNoEncontradoException;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.error.PersistenciaException;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Sala;

/*
 * Christian Gabriel Gutièrrez Gonzàlez
 * Erick Mendieta Rodriguez
 * Emmanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores
 */
public class SalaDAODerbyImp extends AbstractDAO implements SalaDAO {

	public SalaDAODerbyImp() {
		super();
	}
	
	public SalaDAODerbyImp(Connection con) {
		super(con);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void create(Sala s) {
		String sql, part1, part2;
		Statement stmt;
		String salaNom;
		String salaImg;

		// Query inicial
		part1 = "insert into salas (nombre,imagen) ";
		part2 = " values('";

		// Verifica que tenga nombre
		salaNom = s.getNombre();
		if (salaNom == null || salaNom.length() < 1) {
			throw new PersistenciaException("La sala debe tener un nombre");
		}
		// Coloca el nombre en los valores
		part2 += s.getNombre() + "','";
		// Revisa si tiene imagen
		salaImg = s.getImagen();
		if (salaImg == null || salaImg.length() < 4) { // Por lo menos una letra y la extension
			throw new PersistenciaException("La sala debe tener una imagen");
		}
		part2 += s.getImagen() + "')";
		sql = part1 + part2;
		try {
			stmt = this.conexion.createStatement();
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			throw new PersistenciaException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sala read(Sala s) {
		String sql, n, donde;
		Statement stmt;
		Sala sala;
		ResultSet rs;
		int id, nAtributos;

		nAtributos = 0;

		donde = " where ";

		sql = "select * from salas ";
		id = s.getId(); // Consulta el id
		n = s.getNombre(); // Consulta el nombre
		if (id > 0) { // Tenia id
			nAtributos++;
			sql += donde + " id=" + id;
		}
		if (n != null) { // Tenia nombre
			if (nAtributos > 0) { // Si tuvo id
				sql += " and " + "nombre='" + s.getNombre() + "' ";
			} else {
				sql += donde + "nombre='" + s.getNombre() + "' ";
			}
			nAtributos++;
		}
		try {
			stmt = this.conexion.createStatement();
			rs = stmt.executeQuery(sql);
			sala = new Sala();
			while (rs.next()) {
				sala.setId(rs.getInt("id"));
				sala.setNombre(rs.getString("nombre"));
				sala.setImagen(rs.getString("imagen"));
			}
		} catch (SQLException e) {
			throw new PersistenciaException(e); // Convertimos la excepcion
		}
		return sala;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Sala> read() {
		String sql;
		Statement stmt;
		ResultSet rs;
		Sala s;
		List<Sala> salas;
		salas = new ArrayList<Sala>();

		sql = "select * from salas";
		try {
			stmt = this.conexion.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				s = new Sala();
				s.setId(rs.getInt("id"));
				s.setNombre(rs.getString("nombre"));
				s.setImagen(rs.getString("imagen"));
				salas.add(s);
			}
		} catch (Exception e) {
			throw new PersistenciaException(e);
		}
		return salas;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(Sala s, Sala s1) {
	    String sql;
	    Sala sBus;

	    sBus = this.buscaByNombre(s.getNombre());
	    if (sBus == null) {
	        throw new ElementoNoEncontradoException(s.getNombre() + " no se encuentra en la base");
	    }

	    // Utilizar StringBuilder para construir la cadena de actualización
	    StringBuilder updateAttributes = new StringBuilder();

	    // Verificar los valores a actualizar y construir la cadena de actualización
	    if (s1.getNombre() != null) {
	        updateAttributes.append("nombre='").append(s1.getNombre()).append("', ");
	    }

	    if (s1.getImagen() != null && !s1.getImagen().isEmpty()) {
	        updateAttributes.append("imagen='").append(s1.getImagen()).append("', ");
	    }

	    // Verificar si se ha realizado alguna actualización
	    if (updateAttributes.length() > 0) {
	        // Eliminar la coma final y espacios en blanco de la cadena de actualización
	        updateAttributes.delete(updateAttributes.length() - 2, updateAttributes.length());

	        // Utilizar una sentencia preparada para evitar problemas de seguridad y rendimiento
	        sql = "UPDATE salas SET " + updateAttributes + " WHERE id = ?";
	        try (PreparedStatement pstmt = this.conexion.prepareStatement(sql)) {
	            pstmt.setInt(1, sBus.getId());
	            pstmt.executeUpdate();
	        } catch (Exception e) {
	            throw new PersistenciaException(e);
	        }
	    }
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Sala s) {
		Statement stmt;
		String sql;
		Sala sBus;

		sBus = this.buscaByNombre(s.getNombre());
		if (sBus != null) {
			// En la base lo que identifica a una Sala es su ID
			sql = "delete from salas ";
			sql += " where id =" + sBus.getId();
			try {
				stmt = this.conexion.createStatement();
				stmt.executeUpdate(sql);
			} catch (Exception e) {
				throw new PersistenciaException(e);
			}
		} else {
			throw new ElementoNoEncontradoException(s.getNombre() + " no se encuentra en la base");
		}
	}

	/**
	 * <p>
	 * Busca una sala utilizando su nombre.
	 * </p>
	 * <p>
	 * <b>Nota:</b> Es sensible a may&uacute;sculas y min&uacute;sculas.
	 * </p>
	 * 
	 * @param n Nombre de la sala
	 * @return Registro leido de la base
	 */
	private Sala buscaByNombre(String n) {
		String sql;
		Statement stmt;
		ResultSet rs;
		Sala s = null;

		sql = "select * from salas where nombre='" + n + "'";
		try {
			stmt = this.conexion.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				s = new Sala();
				s.setId(rs.getInt("id"));
				s.setNombre(rs.getString("nombre"));
				s.setImagen(rs.getString("imagen"));
			}
		} catch (Exception e) {
			throw new PersistenciaException(e);
		}
		return s;
	}
}
