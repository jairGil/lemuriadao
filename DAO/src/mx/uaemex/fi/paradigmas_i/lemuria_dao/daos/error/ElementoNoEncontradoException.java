/*
 * Christian Gabriel Gutièrrez Gonzàlez
 * Erick Mendieta Rodriguez
 * Emmanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Gair Gil Dolores
 */

package mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.error;

public class ElementoNoEncontradoException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ElementoNoEncontradoException() {
		super();
	}

	public ElementoNoEncontradoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ElementoNoEncontradoException(String message, Throwable cause) {
		super(message, cause);
	}

	public ElementoNoEncontradoException(String message) {
		super(message);
	}

	public ElementoNoEncontradoException(Throwable cause) {
		super(cause);
	}

}
