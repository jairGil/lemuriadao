package mx.uaemex.fi.paradigmas_i.lemuria_dao.daos;
/*Christian Gabriel Gutierrez Gonzalez
 * Erick Mendieta Rodríguez
 * Emanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores*/
import java.util.List;

import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Enemigo;

public interface EnemigosDAO {
	
	void create(Enemigo e);
	Enemigo read(Enemigo e);
	List<Enemigo> read();
	void update(Enemigo e, Enemigo e2);
	void delete(Enemigo e);
	

}
