/*
 * Christian Gabriel Gutièrrez Gonzàlez
 * Erick Mendieta Rodriguez
 * Emmanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores
 */
package mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.AbstractDAO;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.PocionesDAO;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.error.ElementoNoEncontradoException;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.error.PersistenciaException;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Pocion;

public class PocionesDAODerbyImp extends AbstractDAO implements PocionesDAO {

	public PocionesDAODerbyImp(Connection con) {
		super(con);
	}

	public PocionesDAODerbyImp() {
		super();
	}

	@Override
	public void create(Pocion p) {
		String sql, part1, part2;
		Statement stmt;
		String pocionNom;
		String pocionImg;
		int pocionNivel, pocionTipo;

		// Query inicial
		part1 = "insert into pociones (nombre, nivel, imagen, tipo) ";
		part2 = " values('";

		// Verifica que tenga nombre
		pocionNom = p.getNombre();
		if (pocionNom == null || pocionNom.length() < 1) {
			throw new PersistenciaException("La pocion debe tener un nombre");
		}
		// Coloca el nombre en los valores
		part2 += pocionNom + "',";

		// Revisa si tiene un poder
		pocionNivel = p.getNivel();
		if (pocionNivel == 0) {
			throw new PersistenciaException("La pocion debe tener un nivel");
		}
		// Coloca el nivel en los valores
		part2 += pocionNivel + ",'";

		// Revisa si tiene imagen
		pocionImg = p.getImagen();
		if (pocionImg == null || pocionImg.length() < 4) { // Por lo menos una letra y la extension
			throw new PersistenciaException("La pocion debe tener una imagen");
		}
		part2 += pocionImg + "',";

		// Revisa si tiene tipo
		pocionTipo = p.getTipo();
		if (pocionTipo == 0) {
			throw new PersistenciaException("La pocion debe tener un tipo");
		}
		part2 += pocionTipo + ")";

		sql = part1 + part2;
		try {
			stmt = this.conexion.createStatement();
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			throw new PersistenciaException(e);
		}
	}

	@Override
	public Pocion read(Pocion p) {
		String sql, img, donde, n;
		Statement stmt;
		ResultSet rs;
		Pocion pocion;
		int id, nivel, tipo, nAtributos;

		nAtributos = 0;

		donde = " where ";

		sql = "select * from pociones ";

		id = p.getId(); // Consulta el id
		n = p.getNombre();// Consulta el nombre
		nivel = p.getNivel();// Consulta el nivel
		img = p.getImagen(); // Consulta la imagen
		tipo = p.getTipo();// Consulta el tipo

		if (id > 0) { // Tenia id
			nAtributos++;
			sql += donde + " id=" + id;
		}

		if (n != null) { // Tenia nombre
			if (nAtributos > 0) { // Si tuvo id
				sql += " and " + "nombre='" + p.getNombre() + "' ";
			} else {
				sql += donde + "nombre='" + p.getNombre() + "' ";
			}
			nAtributos++;
		}

		if (nivel > 0) { // Tenia nivel
			if (nAtributos > 0) { // Si tuvo id
				sql += " and " + "nivel=" + p.getNivel() + " ";
			} else {
				sql += donde + "nivel=" + p.getNivel() + " ";
			}
			nAtributos++;
		}
		if (img != null) { // Tenia imagen
			if (nAtributos > 0) { // Si tuvo id
				sql += " and " + "imagen='" + p.getImagen() + "' ";
			} else {
				sql += donde + "imagen='" + p.getImagen() + "' ";
			}
			nAtributos++;
		}

		if (tipo > 0) { // Tenia tipo
			if (nAtributos > 0) { // Si tuvo id
				sql += " and tipo=" + p.getTipo() + " ";
			} else {
				sql += donde + "tipo=" + p.getDescripcion() + " ";
			}
			nAtributos++;
		}

		try {
			stmt = this.conexion.createStatement();
			rs = stmt.executeQuery(sql);
			pocion = new Pocion();
			while (rs.next()) {
				pocion.setId(rs.getInt("id"));
				pocion.setNivel(rs.getInt("nivel"));
				pocion.setNombre(rs.getString("nombre"));
				pocion.setImagen(rs.getString("imagen"));
				pocion.setTipo(rs.getInt("tipo"));
			}
		} catch (SQLException e) {
			throw new PersistenciaException(e); // Convertimos la excepcion
		}

		return pocion;

	}

	@Override
	public List<Pocion> read() {
		String sql;
		Statement stmt;
		ResultSet rs;
		Pocion pocion;
		List<Pocion> pociones;
		pociones = new ArrayList<Pocion>();

		sql = "select * from pociones";
		try {
			stmt = this.conexion.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				pocion = new Pocion();
				pocion.setId(rs.getInt("id"));
				pocion.setNivel(rs.getInt("nivel"));
				pocion.setNombre(rs.getString("nombre"));
				pocion.setImagen(rs.getString("imagen"));
				pocion.setTipo(rs.getInt("tipo"));
				pociones.add(pocion);
			}
		} catch (Exception e) {
			throw new PersistenciaException(e);
		}
		return pociones;
	}

	@Override
	public void update(Pocion oldPocion, Pocion newPocion) {
	    String sql;
	    Pocion pBus;

	    pBus = this.buscaByNombre(oldPocion.getNombre());
	    if (pBus != null) {
	        StringBuilder updateAttributes = new StringBuilder();

	        if (newPocion.getNombre() != null) {
	            updateAttributes.append("nombre='").append(newPocion.getNombre()).append("', ");
	        }

	        if (newPocion.getNivel() > 0) {
	            updateAttributes.append("nivel=").append(newPocion.getNivel()).append(", ");
	        }

	        if (newPocion.getImagen() != null) {
	            updateAttributes.append("imagen='").append(newPocion.getImagen()).append("', ");
	        }

	        if (newPocion.getTipo() > 0) {
	            updateAttributes.append("tipo=").append(newPocion.getTipo()).append(", ");
	        }

	        // Verificar si se ha realizado alguna actualización
	        if (updateAttributes.length() > 0) {
	            // Eliminar la coma final y los espacios en blanco de la cadena de actualización
	            updateAttributes.delete(updateAttributes.length() - 2, updateAttributes.length());

	            // Utilizar una sentencia preparada para evitar problemas de seguridad y rendimiento
	            sql = "UPDATE pociones SET " + updateAttributes + " WHERE id = ?";
	            try (PreparedStatement pstmt = this.conexion.prepareStatement(sql)) {
	                pstmt.setInt(1, pBus.getId());
	                pstmt.executeUpdate();
	            } catch (Exception e) {
	                throw new PersistenciaException(e);
	            }
	        }
	    } else {
	        throw new ElementoNoEncontradoException(oldPocion.getNombre() + " no se encuentra en la base");
	    }
	}


	@Override
	public void delete(Pocion p) {
		Statement stmt;
		String sql;
		Pocion pBus;

		pBus = this.buscaByNombre(p.getNombre());
		if (pBus != null) {
			// En la base lo que identifica pocion una pocion es su ID
			sql = "delete from pociones where id=" + pBus.getId();
			try {
				stmt = this.conexion.createStatement();
				stmt.executeUpdate(sql);
			} catch (Exception e) {
				throw new PersistenciaException(e);
			}
		} else {
			throw new ElementoNoEncontradoException(p.getNombre() + " no se encuentra en la base");
		}
	}

	private Pocion buscaByNombre(String nombre) {
		String sql;
		Statement stmt;
		ResultSet rs;
		Pocion pocion = null;

		sql = "select * from pociones where nombre='" + nombre + "'";
		try {
			stmt = this.conexion.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				pocion = new Pocion();
				pocion.setId(rs.getInt("id"));
				pocion.setNivel(rs.getInt("nivel"));
				pocion.setNombre(rs.getString("nombre"));
				pocion.setImagen(rs.getString("imagen"));
				pocion.setTipo(rs.getInt("tipo"));
			}
		} catch (Exception e) {
			throw new PersistenciaException(e);
		}
		return pocion;
	}

}