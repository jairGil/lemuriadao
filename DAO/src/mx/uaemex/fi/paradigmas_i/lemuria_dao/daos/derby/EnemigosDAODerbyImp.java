/*
 * Christian Gabriel Gutièrrez Gonzàlez
 * Erick Mendieta Rodriguez
 * Emmanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores
 */

package mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.AbstractDAO;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.EnemigosDAO;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.error.ElementoNoEncontradoException;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.error.PersistenciaException;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Enemigo;

public class EnemigosDAODerbyImp extends AbstractDAO implements EnemigosDAO {

	public EnemigosDAODerbyImp(Connection con) {
		super(con);
	}

	public EnemigosDAODerbyImp() {
		super();
	}

	@Override
	public void create(Enemigo e) {
		String sql, part1, part2;
		Statement stmt;
		String eneNom;
		int eneVida;
		int eneDefensa;
		int eneAtaque;
		String eneImagen;

		// Query inicial
		part1 = "insert into enemigos (nombre,vida,defensa,ataque,imagen) ";
		part2 = " values('";

		// Verifica que tenga nombre
		eneNom = e.getNombre();
		if (eneNom == null || eneNom.length() < 1) {
			throw new PersistenciaException("El enemigo debe tener un nombre");
		}
		// Coloca el nombre en los valores
		part2 += eneNom + "',";

		// Verifica que tenga Vida válida
		eneVida = e.getVida();
		if (eneVida <= 0 || eneVida > 100) {
			throw new PersistenciaException("Vida inválida para el enemigo");
		}
		// Coloca la vida en los valores
		part2 += eneVida + ",";

		// Revisa si tiene Defensa válida
		eneDefensa = e.getDefensa();
		if (eneDefensa <= 0 || eneDefensa > 100) {
			throw new PersistenciaException("Defensa inválida para el enemigo");
		}
		// Coloca la Defensa en los valores
		part2 += eneDefensa + ",";

		// Revisa si tiene Ataque válida
		eneAtaque = e.getAtaque();
		if (eneDefensa <= 0 || eneDefensa > 100) {
			throw new PersistenciaException("Defensa inválida para el enemigo");
		}
		// Coloca la vida en los valores
		part2 += eneAtaque + ",'";

		// Revisa si tiene imagen
		eneImagen = e.getImagen();
		if (eneImagen == null || eneImagen.length() < 4) { // Por lo menos una letra y la extension
			throw new PersistenciaException("La sala debe tener una imagen");
		}
		part2 += eneImagen + "')";

		sql = part1 + part2;
		try {
			stmt = this.conexion.createStatement();
			stmt.executeUpdate(sql);
		} catch (SQLException ex) {
			throw new PersistenciaException(ex);
		}
	}

	@Override
	public Enemigo read(Enemigo e) {
		String sql, n, donde;
		Statement stmt;
		Enemigo enemigo;
		ResultSet rs;
		int id, nAtributos;

		nAtributos = 0;

		donde = " where ";

		sql = "select * from enemigos ";
		id = e.getId(); // Consulta el id
		n = e.getNombre(); // Consulta el nombre

		if (id > 0) { // Tenia id
			nAtributos++;
			sql += donde + " id=" + id;
		}

		if (n != null) { // Tenia nombre
			if (nAtributos > 0) { // Si tuvo id
				sql += " and " + "nombre='" + e.getNombre() + "' ";
			} else {
				sql += donde + "nombre='" + e.getNombre() + "' ";
			}
			nAtributos++;
		}

		try {
			stmt = this.conexion.createStatement();
			rs = stmt.executeQuery(sql);
			enemigo = new Enemigo();
			while (rs.next()) {
				enemigo.setId(rs.getInt("id"));
				enemigo.setNombre(rs.getString("nombre"));
				enemigo.setVida(rs.getInt("vida"));
				enemigo.setDefensa(rs.getInt("defensa"));
				enemigo.setAtaque(rs.getInt("ataque"));
				enemigo.setImagen(rs.getString("imagen"));
			}
		} catch (SQLException ex) {
			throw new PersistenciaException(ex); // Convertimos la excepcion
		}
		return enemigo;
	}

	@Override
	public List<Enemigo> read() {
		String sql;
		Statement stmt;
		ResultSet rs;
		Enemigo e;
		List<Enemigo> enemigos;
		enemigos = new ArrayList<Enemigo>();

		sql = "select * from enemigos";
		try {
			stmt = this.conexion.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				e = new Enemigo();
				e.setId(rs.getInt("id"));
				e.setNombre(rs.getString("nombre"));
				e.setVida(rs.getInt("vida"));
				e.setDefensa(rs.getInt("defensa"));
				e.setAtaque(rs.getInt("ataque"));
				e.setImagen(rs.getString("imagen"));
				enemigos.add(e);
			}
		} catch (Exception ex) {
			throw new PersistenciaException(ex);
		}
		return enemigos;
	}

	@Override
	public void update(Enemigo oldEnemigo, Enemigo newEnemigo) {
	    String sql;
	    Enemigo eBus;

	    eBus = this.buscaByNombre(oldEnemigo.getNombre());
	    if (eBus != null) {
	        StringBuilder updateAttributes = new StringBuilder();

	        if (newEnemigo.getNombre() != null) {
	            updateAttributes.append("nombre='").append(newEnemigo.getNombre()).append("', ");
	        }

	        if (newEnemigo.getVida() > 0) {
	            updateAttributes.append("vida=").append(newEnemigo.getVida()).append(", ");
	        }

	        if (newEnemigo.getDefensa() > 0) {
	            updateAttributes.append("defensa=").append(newEnemigo.getDefensa()).append(", ");
	        }

	        if (newEnemigo.getAtaque() > 0) {
	            updateAttributes.append("ataque=").append(newEnemigo.getAtaque()).append(", ");
	        }

	        if (newEnemigo.getImagen() != null) {
	            updateAttributes.append("imagen='").append(newEnemigo.getImagen()).append("', ");
	        }

	        // Verificar si se ha realizado alguna actualización
	        if (updateAttributes.length() > 0) {
	            // Eliminar la coma final y los espacios en blanco de la cadena de actualización
	            updateAttributes.delete(updateAttributes.length() - 2, updateAttributes.length());

	            // Utilizar una sentencia preparada para evitar problemas de seguridad y rendimiento
	            sql = "UPDATE enemigos SET " + updateAttributes + " WHERE id = ?";
	            try (PreparedStatement pstmt = this.conexion.prepareStatement(sql)) {
	                pstmt.setInt(1, eBus.getId());
	                pstmt.executeUpdate();
	            } catch (Exception ex) {
	                throw new PersistenciaException(ex);
	            }
	        }
	    } else {
	        throw new ElementoNoEncontradoException(oldEnemigo.getNombre() + " no se encuentra en la base");
	    }
	}


	@Override
	public void delete(Enemigo e) {
		Statement stmt;
		String sql;
		Enemigo eBus;

		eBus = this.buscaByNombre(e.getNombre());
		if (eBus != null) {
			// En la base lo que identifica a una Sala es su ID
			sql = "delete from enemigos ";
			sql += " where id =" + eBus.getId();
			try {
				stmt = this.conexion.createStatement();
				stmt.executeUpdate(sql);
			} catch (Exception ex) {
				throw new PersistenciaException(ex);
			}
		} else {
			throw new ElementoNoEncontradoException(e.getNombre() + " no se encuentra en la base");
		}
	}

	private Enemigo buscaByNombre(String n) {
		String sql;
		Statement stmt;
		ResultSet rs;
		Enemigo e = null;

		sql = "select * from enemigos where nombre='" + n + "'";
		try {
			stmt = this.conexion.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				e = new Enemigo();
				e.setId(rs.getInt("id"));
				e.setNombre(rs.getString("nombre"));
				e.setVida(rs.getInt("vida"));
				e.setDefensa(rs.getInt("defensa"));
				e.setAtaque(rs.getInt("ataque"));
				e.setImagen(rs.getString("imagen"));
			}
		} catch (Exception ex) {
			throw new PersistenciaException(ex);
		}
		return e;
	}

}