package mx.uaemex.fi.paradigmas_i.lemuria_dao.daos;
/*Christian Gabriel Gutierrez Gonzalez
 * Erick Mendieta Rodríguez
 * Emanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores*/
import java.sql.Connection;
import java.sql.SQLException;

public class AbstractDAO {
	String driver = "org.apache.derby.jdbc.EmbeddedDriver";
	String jdbcURL = "jdbc:derby:prueba;create=true";
	protected Connection conexion;
	
	public AbstractDAO() {
		try {
            Class.forName(this.driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        
        try {
            this.conexion = java.sql.DriverManager.getConnection(jdbcURL);
            System.out.println("Base de datos conectada");
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
	public AbstractDAO(Connection con) {
		this.conexion = con;
	}
	
	public void setConexion(Connection con) {
		this.conexion = con;
	}
}
