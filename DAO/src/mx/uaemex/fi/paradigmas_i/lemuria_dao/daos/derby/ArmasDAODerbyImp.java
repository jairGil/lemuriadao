/*
 * Christian Gabriel Gutièrrez Gonzàlez
 * Erick Mendieta Rodriguez
 * Emmanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores
 */
package mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.AbstractDAO;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.ArmasDAO;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.error.ElementoNoEncontradoException;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.error.PersistenciaException;
import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Arma;

public class ArmasDAODerbyImp extends AbstractDAO implements ArmasDAO {
	
	public ArmasDAODerbyImp(Connection con) {
		super(con);
	}

	public ArmasDAODerbyImp() {
		super();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void create(Arma arma) {
		String sql, part1, part2;
		Statement stmt;
		String armaNombre;
		String armaImg;
		String armaDesc;
		int armaPoder;
		
		//Query inicial
		part1 = "insert into armas (poder, nombre, imagen, descripcion) ";
		part2 = " values(";
		
		//Verifica que tenga poder
		armaPoder = arma.getNivel();
		if(armaPoder == 0) {
			throw new PersistenciaException("El arma debe tener un poder");
		}
		//Coloca el nivel en los valores
		part2 += armaPoder + ",'";

		//Revisa si tiene descripcion
		armaNombre = arma.getNombre();
		if(armaNombre == null || armaNombre.length() < 4) { //Por lo menos una letra y la extension
			throw new PersistenciaException("El arma debe tener una descripcion");
		}

		// colocar el nombre en los valores
		part2 += armaNombre + "','";

		//Revisa si tiene imagen
		armaImg = arma.getImagen();
		if(armaImg == null || armaImg.length() < 4) { //Por lo menos una letra y la extension
			throw new PersistenciaException("El arma debe tener una imagen");
		}

		part2 += armaImg + "','";
		
		armaDesc = arma.getDescripcion();
		if(armaDesc == null || armaDesc.length() < 4) { //Por lo menos una letra y la extension
			throw new PersistenciaException("El arma debe tener una descripcion");
		}
		
		part2 += armaDesc + "')";
		
		sql = part1 + part2;
		try {
			stmt = this.conexion.createStatement();
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			throw new PersistenciaException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Arma read(Arma a) {
		String sql, desc, donde;
		Statement stmt;
		ResultSet rs;
		Arma arma;
		int id, nAtributos;
		
		nAtributos = 0;
		
		donde = " where ";
		
		sql = "select * from armas ";

        id = a.getId(); //Consulta el id
		desc = a.getDescripcion(); //Consulta el Descripcion

		if(id > 0) {  //Tenia id
			nAtributos++;
			sql += donde + " id=" + id;
		} 

		if(desc!=null) { //Tenia Descripcion
			if(nAtributos > 0) { //Si tuvo id
				sql += " and descripcion='" + a.getDescripcion() + "' ";
			} else {
				sql += donde + "descripcion='" + a.getDescripcion() + "' ";
			}
			nAtributos++;
		}

		try {
			stmt = this.conexion.createStatement();
			rs = stmt.executeQuery(sql);
			arma = new Arma();
			while(rs.next()) {
				arma.setId(rs.getInt("id"));
				arma.setNivel(rs.getInt("poder"));
				arma.setNombre(rs.getString("nombre"));
				arma.setImagen(rs.getString("imagen"));
				arma.setDescripcion(rs.getString("descripcion"));
			}
		} catch (SQLException e) {
			throw new PersistenciaException(e); //Convertimos la excepcion
		}

		return arma;
    
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Arma> read() {
		String sql;
		Statement stmt;
		ResultSet rs;
		Arma arma;
		List<Arma> armas;
		armas = new ArrayList<Arma>();
		
		sql = "select * from armas";
		try {
			stmt = this.conexion.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				arma = new Arma();
				arma.setId(rs.getInt("id"));
				arma.setNivel(rs.getInt("poder"));
				arma.setNombre(rs.getString("nombre"));
				arma.setImagen(rs.getString("imagen"));
				armas.add(arma);
			}
		}catch(Exception e) {
			throw new PersistenciaException(e);
		}
		return armas;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(Arma oldArma, Arma newArma) {
	    String sql;
	    Arma aBus;

	    aBus = this.buscaByNombre(oldArma.getNombre());
	    if (aBus != null) {
	        StringBuilder updateAttributes = new StringBuilder();

	        if (newArma.getNombre() != null) {
	            updateAttributes.append("nombre='").append(newArma.getNombre()).append("', ");
	        }

	        if (newArma.getNivel() != 0) {
	            updateAttributes.append("poder=").append(newArma.getNivel()).append(", ");
	        }

	        if (newArma.getImagen() != null) {
	            updateAttributes.append("imagen='").append(newArma.getImagen()).append("', ");
	        }

	        if (newArma.getDescripcion() != null) {
	            updateAttributes.append("descripcion='").append(newArma.getDescripcion()).append("', ");
	        }

	        // Verificar si se ha realizado alguna actualización
	        if (updateAttributes.length() > 0) {
	            // Eliminar la coma final y espacios en blanco de la cadena de actualización
		        updateAttributes.delete(updateAttributes.length() - 2, updateAttributes.length());

	            // Utilizar una sentencia preparada para evitar problemas de seguridad y rendimiento
	            sql = "UPDATE armas SET " + updateAttributes + " WHERE id = ?";
	            try (PreparedStatement pstmt = this.conexion.prepareStatement(sql)) {
	                pstmt.setInt(1, aBus.getId());
	                pstmt.executeUpdate();
	            } catch (Exception e) {
	                throw new PersistenciaException(e);
	            }
	        }
	    } else {
	        throw new ElementoNoEncontradoException(oldArma.getNombre() + " no se encuentra en la base");
	    }
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Arma arma) {
		Statement stmt;
		String sql;
		Arma aBus;
		
		aBus = this.buscaByNombre(arma.getNombre());
		if(aBus!=null) {
			// En la base lo que identifica arma una arma es su ID
			sql = "delete from armas where id=" + aBus.getId();
			try {
				stmt = this.conexion.createStatement();
				stmt.executeUpdate(sql);
			}catch(Exception e) {
				throw new PersistenciaException(e);
			}
		}else {
			throw new ElementoNoEncontradoException(arma.getNombre()+" no se encuentra en la base");
		}
	}
    
	/**
	 * <p>
	 * Busca una arma utilizando su Descripcion.
	 * </p>
	 * <p>
	 * <b>Nota:</b> Es sensible arma may&uacute;sculas y min&uacute;sculas.
	 * </p>
	 * @param nombre Descripcion de la arma
	 * @return Registro leido de la base
	 */
	private Arma buscaByNombre(String nombre) {
		String sql;
		Statement stmt;
		ResultSet rs;
		Arma arma = null;
		
		sql = "select * from armas where nombre='"+nombre+"'";
		try {
			stmt = this.conexion.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				arma = new Arma();
				arma.setId(rs.getInt("id"));
				arma.setDescripcion(rs.getString("nombre"));
				arma.setNivel(rs.getInt("poder"));
				arma.setImagen(rs.getString("imagen"));
				arma.setDescripcion(rs.getString("descripcion"));
			}
		}catch(Exception e) {
			throw new PersistenciaException(e);
		}
		return arma;
	}
}
