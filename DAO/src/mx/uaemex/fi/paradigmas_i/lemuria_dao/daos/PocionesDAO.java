package mx.uaemex.fi.paradigmas_i.lemuria_dao.daos;
/*Christian Gabriel Gutierrez Gonzalez
 * Erick Mendieta Rodríguez
 * Emanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores*/
import java.util.List;

import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Pocion;

public interface PocionesDAO {
	
	void create(Pocion p);
	Pocion read(Pocion p);
	List<Pocion> read();
	void update(Pocion p, Pocion p2);
	void delete(Pocion p);
	

}
