package mx.uaemex.fi.paradigmas_i.lemuria_dao.daos;
/*Christian Gabriel Gutierrez Gonzalez
 * Erick Mendieta Rodríguez
 * Emanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores*/
import java.util.List;

import mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs.Sala;

public interface SalaDAO {
	
	void create(Sala s);
	Sala read(Sala s);
	List<Sala> read();
	void update(Sala s, Sala s1);
	void delete(Sala s);
	

}
