/*
 * Christian Gabriel Gutièrrez Gonzàlez
 * Erick Mendieta Rodriguez
 * Emmanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores
 */

package mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Derby_conexion {

	public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		
		String driver = "org.apache.derby.jdbc.EmbeddedDriver";
		String jdbcURL = "jdbc:derby:prueba;create=true";
		Class.forName(driver);
		try {
			
			Connection connection = DriverManager.getConnection(jdbcURL);
			System.out.println("Base de datos creada");
		} catch (SQLException e) {

			e.printStackTrace();
		}
		

	}

}
