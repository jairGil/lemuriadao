/*
 * Christian Gabriel Gutièrrez Gonzàlez
 * Erick Mendieta Rodriguez
 * Emmanuel Cano Felix
 * Paulina Nicolas Rojas
 * Josue Jair Gil Dolores
 */
module lemuria {
	exports mx.uaemex.fi.paradigmas_i.lemuria_dao.daos;
    exports mx.uaemex.fi.paradigmas_i.lemuria_dao.transfer_objs;
    exports mx.uaemex.fi.paradigmas_i.lemuria_dao.error;
    exports mx.uaemex.fi.paradigmas_i.lemuria_dao.daos.derby;
    requires transitive java.sql;
}